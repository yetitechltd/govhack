! function(e) {
    var t = {};

    function n(r) { if (t[r]) return t[r].exports; var o = t[r] = { i: r, l: !1, exports: {} }; return e[r].call(o.exports, o, o.exports, n), o.l = !0, o.exports }
    n.m = e, n.c = t, n.d = function(e, t, r) { n.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: r }) }, n.r = function(e) { "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 }) }, n.t = function(e, t) {
        if (1 & t && (e = n(e)), 8 & t) return e;
        if (4 & t && "object" == typeof e && e && e.__esModule) return e;
        var r = Object.create(null);
        if (n.r(r), Object.defineProperty(r, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e)
            for (var o in e) n.d(r, o, function(t) { return e[t] }.bind(null, o));
        return r
    }, n.n = function(e) { var t = e && e.__esModule ? function() { return e.default } : function() { return e }; return n.d(t, "a", t), t }, n.o = function(e, t) { return Object.prototype.hasOwnProperty.call(e, t) }, n.p = "", n(n.s = 10)
}([function(e, t, n) {
    "use strict";
    var r = n(2),
        o = n(27),
        i = n(31),
        s = n(32),
        a = function() {
            function e(e) { this._isScalar = !1, e && (this._subscribe = e) }
            return e.prototype.lift = function(t) { var n = new e; return n.source = this, n.operator = t, n }, e.prototype.subscribe = function(e, t, n) {
                var r = this.operator,
                    i = o.toSubscriber(e, t, n);
                if (r ? r.call(i, this.source) : i.add(this.source || !i.syncErrorThrowable ? this._subscribe(i) : this._trySubscribe(i)), i.syncErrorThrowable && (i.syncErrorThrowable = !1, i.syncErrorThrown)) throw i.syncErrorValue;
                return i
            }, e.prototype._trySubscribe = function(e) { try { return this._subscribe(e) } catch (t) { e.syncErrorThrown = !0, e.syncErrorValue = t, e.error(t) } }, e.prototype.forEach = function(e, t) {
                var n = this;
                if (t || (r.root.Rx && r.root.Rx.config && r.root.Rx.config.Promise ? t = r.root.Rx.config.Promise : r.root.Promise && (t = r.root.Promise)), !t) throw new Error("no Promise impl found");
                return new t((function(t, r) {
                    var o;
                    o = n.subscribe((function(t) { if (o) try { e(t) } catch (e) { r(e), o.unsubscribe() } else e(t) }), r, t)
                }))
            }, e.prototype._subscribe = function(e) { return this.source.subscribe(e) }, e.prototype[i.observable] = function() { return this }, e.prototype.pipe = function() { for (var e = [], t = 0; t < arguments.length; t++) e[t - 0] = arguments[t]; return 0 === e.length ? this : s.pipeFromArray(e)(this) }, e.prototype.toPromise = function(e) {
                var t = this;
                if (e || (r.root.Rx && r.root.Rx.config && r.root.Rx.config.Promise ? e = r.root.Rx.config.Promise : r.root.Promise && (e = r.root.Promise)), !e) throw new Error("no Promise impl found");
                return new e((function(e, n) {
                    var r;
                    t.subscribe((function(e) { return r = e }), (function(e) { return n(e) }), (function() { return e(r) }))
                }))
            }, e.create = function(t) { return new e(t) }, e
        }();
    t.Observable = a
}, function(e, t, n) {
    "use strict";
    var r = n(28),
        o = n(29),
        i = n(4),
        s = n(7),
        a = n(5),
        c = n(30),
        u = function() {
            function e(e) { this.closed = !1, this._parent = null, this._parents = null, this._subscriptions = null, e && (this._unsubscribe = e) }
            var t;
            return e.prototype.unsubscribe = function() {
                var e, t = !1;
                if (!this.closed) {
                    var n = this._parent,
                        u = this._parents,
                        p = this._unsubscribe,
                        f = this._subscriptions;
                    this.closed = !0, this._parent = null, this._parents = null, this._subscriptions = null;
                    for (var d = -1, h = u ? u.length : 0; n;) n.remove(this), n = ++d < h && u[d] || null;
                    if (i.isFunction(p)) s.tryCatch(p).call(this) === a.errorObject && (t = !0, e = e || (a.errorObject.e instanceof c.UnsubscriptionError ? l(a.errorObject.e.errors) : [a.errorObject.e]));
                    if (r.isArray(f))
                        for (d = -1, h = f.length; ++d < h;) {
                            var y = f[d];
                            if (o.isObject(y))
                                if (s.tryCatch(y.unsubscribe).call(y) === a.errorObject) {
                                    t = !0, e = e || [];
                                    var v = a.errorObject.e;
                                    v instanceof c.UnsubscriptionError ? e = e.concat(l(v.errors)) : e.push(v)
                                }
                        }
                    if (t) throw new c.UnsubscriptionError(e)
                }
            }, e.prototype.add = function(t) {
                if (!t || t === e.EMPTY) return e.EMPTY;
                if (t === this) return this;
                var n = t;
                switch (typeof t) {
                    case "function":
                        n = new e(t);
                    case "object":
                        if (n.closed || "function" != typeof n.unsubscribe) return n;
                        if (this.closed) return n.unsubscribe(), n;
                        if ("function" != typeof n._addParent) {
                            var r = n;
                            (n = new e)._subscriptions = [r]
                        }
                        break;
                    default:
                        throw new Error("unrecognized teardown " + t + " added to Subscription.")
                }
                return (this._subscriptions || (this._subscriptions = [])).push(n), n._addParent(this), n
            }, e.prototype.remove = function(e) { var t = this._subscriptions; if (t) { var n = t.indexOf(e); - 1 !== n && t.splice(n, 1) } }, e.prototype._addParent = function(e) {
                var t = this._parent,
                    n = this._parents;
                t && t !== e ? n ? -1 === n.indexOf(e) && n.push(e) : this._parents = [e] : this._parent = e
            }, e.EMPTY = ((t = new e).closed = !0, t), e
        }();

    function l(e) { return e.reduce((function(e, t) { return e.concat(t instanceof c.UnsubscriptionError ? t.errors : t) }), []) }
    t.Subscription = u
}, function(e, t, n) {
    "use strict";
    (function(e) {
        var n = "undefined" != typeof window && window,
            r = "undefined" != typeof self && "undefined" != typeof WorkerGlobalScope && self instanceof WorkerGlobalScope && self,
            o = n || void 0 !== e && e || r;
        t.root = o,
            function() { if (!o) throw new Error("RxJS could not find any global context (window, self, global)") }()
    }).call(this, n(26))
}, function(e, t, n) {
    "use strict";
    var r = this && this.__extends || function(e, t) {
            for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);

            function r() { this.constructor = e }
            e.prototype = null === t ? Object.create(t) : (r.prototype = t.prototype, new r)
        },
        o = n(4),
        i = n(1),
        s = n(8),
        a = n(6),
        c = function(e) {
            function t(t, n, r) {
                switch (e.call(this), this.syncErrorValue = null, this.syncErrorThrown = !1, this.syncErrorThrowable = !1, this.isStopped = !1, arguments.length) {
                    case 0:
                        this.destination = s.empty;
                        break;
                    case 1:
                        if (!t) { this.destination = s.empty; break }
                        if ("object" == typeof t) {
                            if (l(t)) {
                                var o = t[a.rxSubscriber]();
                                this.syncErrorThrowable = o.syncErrorThrowable, this.destination = o, o.add(this)
                            } else this.syncErrorThrowable = !0, this.destination = new u(this, t);
                            break
                        }
                    default:
                        this.syncErrorThrowable = !0, this.destination = new u(this, t, n, r)
                }
            }
            return r(t, e), t.prototype[a.rxSubscriber] = function() { return this }, t.create = function(e, n, r) { var o = new t(e, n, r); return o.syncErrorThrowable = !1, o }, t.prototype.next = function(e) { this.isStopped || this._next(e) }, t.prototype.error = function(e) { this.isStopped || (this.isStopped = !0, this._error(e)) }, t.prototype.complete = function() { this.isStopped || (this.isStopped = !0, this._complete()) }, t.prototype.unsubscribe = function() { this.closed || (this.isStopped = !0, e.prototype.unsubscribe.call(this)) }, t.prototype._next = function(e) { this.destination.next(e) }, t.prototype._error = function(e) { this.destination.error(e), this.unsubscribe() }, t.prototype._complete = function() { this.destination.complete(), this.unsubscribe() }, t.prototype._unsubscribeAndRecycle = function() {
                var e = this._parent,
                    t = this._parents;
                return this._parent = null, this._parents = null, this.unsubscribe(), this.closed = !1, this.isStopped = !1, this._parent = e, this._parents = t, this
            }, t
        }(i.Subscription);
    t.Subscriber = c;
    var u = function(e) {
        function t(t, n, r, i) {
            var a;
            e.call(this), this._parentSubscriber = t;
            var c = this;
            o.isFunction(n) ? a = n : n && (a = n.next, r = n.error, i = n.complete, n !== s.empty && (c = Object.create(n), o.isFunction(c.unsubscribe) && this.add(c.unsubscribe.bind(c)), c.unsubscribe = this.unsubscribe.bind(this))), this._context = c, this._next = a, this._error = r, this._complete = i
        }
        return r(t, e), t.prototype.next = function(e) {
            if (!this.isStopped && this._next) {
                var t = this._parentSubscriber;
                t.syncErrorThrowable ? this.__tryOrSetError(t, this._next, e) && this.unsubscribe() : this.__tryOrUnsub(this._next, e)
            }
        }, t.prototype.error = function(e) {
            if (!this.isStopped) {
                var t = this._parentSubscriber;
                if (this._error) t.syncErrorThrowable ? (this.__tryOrSetError(t, this._error, e), this.unsubscribe()) : (this.__tryOrUnsub(this._error, e), this.unsubscribe());
                else {
                    if (!t.syncErrorThrowable) throw this.unsubscribe(), e;
                    t.syncErrorValue = e, t.syncErrorThrown = !0, this.unsubscribe()
                }
            }
        }, t.prototype.complete = function() {
            var e = this;
            if (!this.isStopped) {
                var t = this._parentSubscriber;
                if (this._complete) {
                    var n = function() { return e._complete.call(e._context) };
                    t.syncErrorThrowable ? (this.__tryOrSetError(t, n), this.unsubscribe()) : (this.__tryOrUnsub(n), this.unsubscribe())
                } else this.unsubscribe()
            }
        }, t.prototype.__tryOrUnsub = function(e, t) { try { e.call(this._context, t) } catch (e) { throw this.unsubscribe(), e } }, t.prototype.__tryOrSetError = function(e, t, n) { try { t.call(this._context, n) } catch (t) { return e.syncErrorValue = t, e.syncErrorThrown = !0, !0 } return !1 }, t.prototype._unsubscribe = function() {
            var e = this._parentSubscriber;
            this._context = null, this._parentSubscriber = null, e.unsubscribe()
        }, t
    }(c);

    function l(e) { return e instanceof c || "syncErrorThrowable" in e && e[a.rxSubscriber] }
}, function(e, t, n) {
    "use strict";
    t.isFunction = function(e) { return "function" == typeof e }
}, function(e, t, n) {
    "use strict";
    t.errorObject = { e: {} }
}, function(e, t, n) {
    "use strict";
    var r = n(2).root.Symbol;
    t.rxSubscriber = "function" == typeof r && "function" == typeof r.for ? r.for("rxSubscriber") : "@@rxSubscriber", t.$$rxSubscriber = t.rxSubscriber
}, function(e, t, n) {
    "use strict";
    var r, o = n(5);

    function i() { try { return r.apply(this, arguments) } catch (e) { return o.errorObject.e = e, o.errorObject } }
    t.tryCatch = function(e) { return r = e, i }
}, function(e, t, n) {
    "use strict";
    t.empty = { closed: !0, next: function(e) {}, error: function(e) { throw e }, complete: function() {} }
}, function(e, t, n) {
    var r; /*! jQuery v3.5.1 | (c) JS Foundation and other contributors | jquery.org/license */
    ! function(t, n) { "use strict"; "object" == typeof e.exports ? e.exports = t.document ? n(t, !0) : function(e) { if (!e.document) throw new Error("jQuery requires a window with a document"); return n(e) } : n(t) }("undefined" != typeof window ? window : this, (function(n, o) {
        "use strict";
        var i = [],
            s = Object.getPrototypeOf,
            a = i.slice,
            c = i.flat ? function(e) { return i.flat.call(e) } : function(e) { return i.concat.apply([], e) },
            u = i.push,
            l = i.indexOf,
            p = {},
            f = p.toString,
            d = p.hasOwnProperty,
            h = d.toString,
            y = h.call(Object),
            v = {},
            g = function(e) { return "function" == typeof e && "number" != typeof e.nodeType },
            m = function(e) { return null != e && e === e.window },
            b = n.document,
            w = { type: !0, src: !0, nonce: !0, noModule: !0 };

        function x(e, t, n) {
            var r, o, i = (n = n || b).createElement("script");
            if (i.text = e, t)
                for (r in w)(o = t[r] || t.getAttribute && t.getAttribute(r)) && i.setAttribute(r, o);
            n.head.appendChild(i).parentNode.removeChild(i)
        }

        function T(e) { return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? p[f.call(e)] || "object" : typeof e }
        var S = "3.5.1",
            E = function(e, t) { return new E.fn.init(e, t) };

        function C(e) {
            var t = !!e && "length" in e && e.length,
                n = T(e);
            return !g(e) && !m(e) && ("array" === n || 0 === t || "number" == typeof t && 0 < t && t - 1 in e)
        }
        E.fn = E.prototype = {
            jquery: S,
            constructor: E,
            length: 0,
            toArray: function() { return a.call(this) },
            get: function(e) { return null == e ? a.call(this) : e < 0 ? this[e + this.length] : this[e] },
            pushStack: function(e) { var t = E.merge(this.constructor(), e); return t.prevObject = this, t },
            each: function(e) { return E.each(this, e) },
            map: function(e) { return this.pushStack(E.map(this, (function(t, n) { return e.call(t, n, t) }))) },
            slice: function() { return this.pushStack(a.apply(this, arguments)) },
            first: function() { return this.eq(0) },
            last: function() { return this.eq(-1) },
            even: function() { return this.pushStack(E.grep(this, (function(e, t) { return (t + 1) % 2 }))) },
            odd: function() { return this.pushStack(E.grep(this, (function(e, t) { return t % 2 }))) },
            eq: function(e) {
                var t = this.length,
                    n = +e + (e < 0 ? t : 0);
                return this.pushStack(0 <= n && n < t ? [this[n]] : [])
            },
            end: function() { return this.prevObject || this.constructor() },
            push: u,
            sort: i.sort,
            splice: i.splice
        }, E.extend = E.fn.extend = function() {
            var e, t, n, r, o, i, s = arguments[0] || {},
                a = 1,
                c = arguments.length,
                u = !1;
            for ("boolean" == typeof s && (u = s, s = arguments[a] || {}, a++), "object" == typeof s || g(s) || (s = {}), a === c && (s = this, a--); a < c; a++)
                if (null != (e = arguments[a]))
                    for (t in e) r = e[t], "__proto__" !== t && s !== r && (u && r && (E.isPlainObject(r) || (o = Array.isArray(r))) ? (n = s[t], i = o && !Array.isArray(n) ? [] : o || E.isPlainObject(n) ? n : {}, o = !1, s[t] = E.extend(u, i, r)) : void 0 !== r && (s[t] = r));
            return s
        }, E.extend({
            expando: "jQuery" + (S + Math.random()).replace(/\D/g, ""),
            isReady: !0,
            error: function(e) { throw new Error(e) },
            noop: function() {},
            isPlainObject: function(e) { var t, n; return !(!e || "[object Object]" !== f.call(e) || (t = s(e)) && ("function" != typeof(n = d.call(t, "constructor") && t.constructor) || h.call(n) !== y)) },
            isEmptyObject: function(e) { var t; for (t in e) return !1; return !0 },
            globalEval: function(e, t, n) { x(e, { nonce: t && t.nonce }, n) },
            each: function(e, t) {
                var n, r = 0;
                if (C(e))
                    for (n = e.length; r < n && !1 !== t.call(e[r], r, e[r]); r++);
                else
                    for (r in e)
                        if (!1 === t.call(e[r], r, e[r])) break; return e
            },
            makeArray: function(e, t) { var n = t || []; return null != e && (C(Object(e)) ? E.merge(n, "string" == typeof e ? [e] : e) : u.call(n, e)), n },
            inArray: function(e, t, n) { return null == t ? -1 : l.call(t, e, n) },
            merge: function(e, t) { for (var n = +t.length, r = 0, o = e.length; r < n; r++) e[o++] = t[r]; return e.length = o, e },
            grep: function(e, t, n) { for (var r = [], o = 0, i = e.length, s = !n; o < i; o++) !t(e[o], o) !== s && r.push(e[o]); return r },
            map: function(e, t, n) {
                var r, o, i = 0,
                    s = [];
                if (C(e))
                    for (r = e.length; i < r; i++) null != (o = t(e[i], i, n)) && s.push(o);
                else
                    for (i in e) null != (o = t(e[i], i, n)) && s.push(o);
                return c(s)
            },
            guid: 1,
            support: v
        }), "function" == typeof Symbol && (E.fn[Symbol.iterator] = i[Symbol.iterator]), E.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), (function(e, t) { p["[object " + t + "]"] = t.toLowerCase() }));
        var k = function(e) {
            var t, n, r, o, i, s, a, c, u, l, p, f, d, h, y, v, g, m, b, w = "sizzle" + 1 * new Date,
                x = e.document,
                T = 0,
                S = 0,
                E = ce(),
                C = ce(),
                k = ce(),
                j = ce(),
                A = function(e, t) { return e === t && (p = !0), 0 },
                N = {}.hasOwnProperty,
                O = [],
                D = O.pop,
                _ = O.push,
                L = O.push,
                q = O.slice,
                P = function(e, t) {
                    for (var n = 0, r = e.length; n < r; n++)
                        if (e[n] === t) return n;
                    return -1
                },
                I = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
                R = "[\\x20\\t\\r\\n\\f]",
                H = "(?:\\\\[\\da-fA-F]{1,6}" + R + "?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",
                W = "\\[" + R + "*(" + H + ")(?:" + R + "*([*^$|!~]?=)" + R + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + H + "))|)" + R + "*\\]",
                M = ":(" + H + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + W + ")*)|.*)\\)|)",
                F = new RegExp(R + "+", "g"),
                B = new RegExp("^" + R + "+|((?:^|[^\\\\])(?:\\\\.)*)" + R + "+$", "g"),
                $ = new RegExp("^" + R + "*," + R + "*"),
                U = new RegExp("^" + R + "*([>+~]|" + R + ")" + R + "*"),
                z = new RegExp(R + "|>"),
                Y = new RegExp(M),
                G = new RegExp("^" + H + "$"),
                X = { ID: new RegExp("^#(" + H + ")"), CLASS: new RegExp("^\\.(" + H + ")"), TAG: new RegExp("^(" + H + "|[*])"), ATTR: new RegExp("^" + W), PSEUDO: new RegExp("^" + M), CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + R + "*(even|odd|(([+-]|)(\\d*)n|)" + R + "*(?:([+-]|)" + R + "*(\\d+)|))" + R + "*\\)|)", "i"), bool: new RegExp("^(?:" + I + ")$", "i"), needsContext: new RegExp("^" + R + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + R + "*((?:-\\d)?\\d*)" + R + "*\\)|)(?=[^-]|$)", "i") },
                V = /HTML$/i,
                J = /^(?:input|select|textarea|button)$/i,
                Q = /^h\d$/i,
                K = /^[^{]+\{\s*\[native \w/,
                Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                ee = /[+~]/,
                te = new RegExp("\\\\[\\da-fA-F]{1,6}" + R + "?|\\\\([^\\r\\n\\f])", "g"),
                ne = function(e, t) { var n = "0x" + e.slice(1) - 65536; return t || (n < 0 ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320)) },
                re = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
                oe = function(e, t) { return t ? "\0" === e ? "ï¿½" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e },
                ie = function() { f() },
                se = we((function(e) { return !0 === e.disabled && "fieldset" === e.nodeName.toLowerCase() }), { dir: "parentNode", next: "legend" });
            try { L.apply(O = q.call(x.childNodes), x.childNodes), O[x.childNodes.length].nodeType } catch (t) {
                L = {
                    apply: O.length ? function(e, t) { _.apply(e, q.call(t)) } : function(e, t) {
                        for (var n = e.length, r = 0; e[n++] = t[r++];);
                        e.length = n - 1
                    }
                }
            }

            function ae(e, t, r, o) {
                var i, a, u, l, p, h, g, m = t && t.ownerDocument,
                    x = t ? t.nodeType : 9;
                if (r = r || [], "string" != typeof e || !e || 1 !== x && 9 !== x && 11 !== x) return r;
                if (!o && (f(t), t = t || d, y)) {
                    if (11 !== x && (p = Z.exec(e)))
                        if (i = p[1]) { if (9 === x) { if (!(u = t.getElementById(i))) return r; if (u.id === i) return r.push(u), r } else if (m && (u = m.getElementById(i)) && b(t, u) && u.id === i) return r.push(u), r } else { if (p[2]) return L.apply(r, t.getElementsByTagName(e)), r; if ((i = p[3]) && n.getElementsByClassName && t.getElementsByClassName) return L.apply(r, t.getElementsByClassName(i)), r }
                    if (n.qsa && !j[e + " "] && (!v || !v.test(e)) && (1 !== x || "object" !== t.nodeName.toLowerCase())) {
                        if (g = e, m = t, 1 === x && (z.test(e) || U.test(e))) {
                            for ((m = ee.test(e) && ge(t.parentNode) || t) === t && n.scope || ((l = t.getAttribute("id")) ? l = l.replace(re, oe) : t.setAttribute("id", l = w)), a = (h = s(e)).length; a--;) h[a] = (l ? "#" + l : ":scope") + " " + be(h[a]);
                            g = h.join(",")
                        }
                        try { return L.apply(r, m.querySelectorAll(g)), r } catch (t) { j(e, !0) } finally { l === w && t.removeAttribute("id") }
                    }
                }
                return c(e.replace(B, "$1"), t, r, o)
            }

            function ce() { var e = []; return function t(n, o) { return e.push(n + " ") > r.cacheLength && delete t[e.shift()], t[n + " "] = o } }

            function ue(e) { return e[w] = !0, e }

            function le(e) { var t = d.createElement("fieldset"); try { return !!e(t) } catch (e) { return !1 } finally { t.parentNode && t.parentNode.removeChild(t), t = null } }

            function pe(e, t) { for (var n = e.split("|"), o = n.length; o--;) r.attrHandle[n[o]] = t }

            function fe(e, t) {
                var n = t && e,
                    r = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
                if (r) return r;
                if (n)
                    for (; n = n.nextSibling;)
                        if (n === t) return -1;
                return e ? 1 : -1
            }

            function de(e) { return function(t) { return "input" === t.nodeName.toLowerCase() && t.type === e } }

            function he(e) { return function(t) { var n = t.nodeName.toLowerCase(); return ("input" === n || "button" === n) && t.type === e } }

            function ye(e) { return function(t) { return "form" in t ? t.parentNode && !1 === t.disabled ? "label" in t ? "label" in t.parentNode ? t.parentNode.disabled === e : t.disabled === e : t.isDisabled === e || t.isDisabled !== !e && se(t) === e : t.disabled === e : "label" in t && t.disabled === e } }

            function ve(e) { return ue((function(t) { return t = +t, ue((function(n, r) { for (var o, i = e([], n.length, t), s = i.length; s--;) n[o = i[s]] && (n[o] = !(r[o] = n[o])) })) })) }

            function ge(e) { return e && void 0 !== e.getElementsByTagName && e }
            for (t in n = ae.support = {}, i = ae.isXML = function(e) {
                    var t = e.namespaceURI,
                        n = (e.ownerDocument || e).documentElement;
                    return !V.test(t || n && n.nodeName || "HTML")
                }, f = ae.setDocument = function(e) {
                    var t, o, s = e ? e.ownerDocument || e : x;
                    return s != d && 9 === s.nodeType && s.documentElement && (h = (d = s).documentElement, y = !i(d), x != d && (o = d.defaultView) && o.top !== o && (o.addEventListener ? o.addEventListener("unload", ie, !1) : o.attachEvent && o.attachEvent("onunload", ie)), n.scope = le((function(e) { return h.appendChild(e).appendChild(d.createElement("div")), void 0 !== e.querySelectorAll && !e.querySelectorAll(":scope fieldset div").length })), n.attributes = le((function(e) { return e.className = "i", !e.getAttribute("className") })), n.getElementsByTagName = le((function(e) { return e.appendChild(d.createComment("")), !e.getElementsByTagName("*").length })), n.getElementsByClassName = K.test(d.getElementsByClassName), n.getById = le((function(e) { return h.appendChild(e).id = w, !d.getElementsByName || !d.getElementsByName(w).length })), n.getById ? (r.filter.ID = function(e) { var t = e.replace(te, ne); return function(e) { return e.getAttribute("id") === t } }, r.find.ID = function(e, t) { if (void 0 !== t.getElementById && y) { var n = t.getElementById(e); return n ? [n] : [] } }) : (r.filter.ID = function(e) { var t = e.replace(te, ne); return function(e) { var n = void 0 !== e.getAttributeNode && e.getAttributeNode("id"); return n && n.value === t } }, r.find.ID = function(e, t) {
                        if (void 0 !== t.getElementById && y) {
                            var n, r, o, i = t.getElementById(e);
                            if (i) {
                                if ((n = i.getAttributeNode("id")) && n.value === e) return [i];
                                for (o = t.getElementsByName(e), r = 0; i = o[r++];)
                                    if ((n = i.getAttributeNode("id")) && n.value === e) return [i]
                            }
                            return []
                        }
                    }), r.find.TAG = n.getElementsByTagName ? function(e, t) { return void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e) : n.qsa ? t.querySelectorAll(e) : void 0 } : function(e, t) {
                        var n, r = [],
                            o = 0,
                            i = t.getElementsByTagName(e);
                        if ("*" === e) { for (; n = i[o++];) 1 === n.nodeType && r.push(n); return r }
                        return i
                    }, r.find.CLASS = n.getElementsByClassName && function(e, t) { if (void 0 !== t.getElementsByClassName && y) return t.getElementsByClassName(e) }, g = [], v = [], (n.qsa = K.test(d.querySelectorAll)) && (le((function(e) {
                        var t;
                        h.appendChild(e).innerHTML = "<a id='" + w + "'></a><select id='" + w + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && v.push("[*^$]=" + R + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || v.push("\\[" + R + "*(?:value|" + I + ")"), e.querySelectorAll("[id~=" + w + "-]").length || v.push("~="), (t = d.createElement("input")).setAttribute("name", ""), e.appendChild(t), e.querySelectorAll("[name='']").length || v.push("\\[" + R + "*name" + R + "*=" + R + "*(?:''|\"\")"), e.querySelectorAll(":checked").length || v.push(":checked"), e.querySelectorAll("a#" + w + "+*").length || v.push(".#.+[+~]"), e.querySelectorAll("\\\f"), v.push("[\\r\\n\\f]")
                    })), le((function(e) {
                        e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                        var t = d.createElement("input");
                        t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && v.push("name" + R + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && v.push(":enabled", ":disabled"), h.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && v.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), v.push(",.*:")
                    }))), (n.matchesSelector = K.test(m = h.matches || h.webkitMatchesSelector || h.mozMatchesSelector || h.oMatchesSelector || h.msMatchesSelector)) && le((function(e) { n.disconnectedMatch = m.call(e, "*"), m.call(e, "[s!='']:x"), g.push("!=", M) })), v = v.length && new RegExp(v.join("|")), g = g.length && new RegExp(g.join("|")), t = K.test(h.compareDocumentPosition), b = t || K.test(h.contains) ? function(e, t) {
                        var n = 9 === e.nodeType ? e.documentElement : e,
                            r = t && t.parentNode;
                        return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r)))
                    } : function(e, t) {
                        if (t)
                            for (; t = t.parentNode;)
                                if (t === e) return !0;
                        return !1
                    }, A = t ? function(e, t) { if (e === t) return p = !0, 0; var r = !e.compareDocumentPosition - !t.compareDocumentPosition; return r || (1 & (r = (e.ownerDocument || e) == (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !n.sortDetached && t.compareDocumentPosition(e) === r ? e == d || e.ownerDocument == x && b(x, e) ? -1 : t == d || t.ownerDocument == x && b(x, t) ? 1 : l ? P(l, e) - P(l, t) : 0 : 4 & r ? -1 : 1) } : function(e, t) {
                        if (e === t) return p = !0, 0;
                        var n, r = 0,
                            o = e.parentNode,
                            i = t.parentNode,
                            s = [e],
                            a = [t];
                        if (!o || !i) return e == d ? -1 : t == d ? 1 : o ? -1 : i ? 1 : l ? P(l, e) - P(l, t) : 0;
                        if (o === i) return fe(e, t);
                        for (n = e; n = n.parentNode;) s.unshift(n);
                        for (n = t; n = n.parentNode;) a.unshift(n);
                        for (; s[r] === a[r];) r++;
                        return r ? fe(s[r], a[r]) : s[r] == x ? -1 : a[r] == x ? 1 : 0
                    }), d
                }, ae.matches = function(e, t) { return ae(e, null, null, t) }, ae.matchesSelector = function(e, t) {
                    if (f(e), n.matchesSelector && y && !j[t + " "] && (!g || !g.test(t)) && (!v || !v.test(t))) try { var r = m.call(e, t); if (r || n.disconnectedMatch || e.document && 11 !== e.document.nodeType) return r } catch (e) { j(t, !0) }
                    return 0 < ae(t, d, null, [e]).length
                }, ae.contains = function(e, t) { return (e.ownerDocument || e) != d && f(e), b(e, t) }, ae.attr = function(e, t) {
                    (e.ownerDocument || e) != d && f(e);
                    var o = r.attrHandle[t.toLowerCase()],
                        i = o && N.call(r.attrHandle, t.toLowerCase()) ? o(e, t, !y) : void 0;
                    return void 0 !== i ? i : n.attributes || !y ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
                }, ae.escape = function(e) { return (e + "").replace(re, oe) }, ae.error = function(e) { throw new Error("Syntax error, unrecognized expression: " + e) }, ae.uniqueSort = function(e) {
                    var t, r = [],
                        o = 0,
                        i = 0;
                    if (p = !n.detectDuplicates, l = !n.sortStable && e.slice(0), e.sort(A), p) { for (; t = e[i++];) t === e[i] && (o = r.push(i)); for (; o--;) e.splice(r[o], 1) }
                    return l = null, e
                }, o = ae.getText = function(e) {
                    var t, n = "",
                        r = 0,
                        i = e.nodeType;
                    if (i) { if (1 === i || 9 === i || 11 === i) { if ("string" == typeof e.textContent) return e.textContent; for (e = e.firstChild; e; e = e.nextSibling) n += o(e) } else if (3 === i || 4 === i) return e.nodeValue } else
                        for (; t = e[r++];) n += o(t);
                    return n
                }, (r = ae.selectors = {
                    cacheLength: 50,
                    createPseudo: ue,
                    match: X,
                    attrHandle: {},
                    find: {},
                    relative: { ">": { dir: "parentNode", first: !0 }, " ": { dir: "parentNode" }, "+": { dir: "previousSibling", first: !0 }, "~": { dir: "previousSibling" } },
                    preFilter: { ATTR: function(e) { return e[1] = e[1].replace(te, ne), e[3] = (e[3] || e[4] || e[5] || "").replace(te, ne), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4) }, CHILD: function(e) { return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || ae.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && ae.error(e[0]), e }, PSEUDO: function(e) { var t, n = !e[6] && e[2]; return X.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && Y.test(n) && (t = s(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3)) } },
                    filter: {
                        TAG: function(e) { var t = e.replace(te, ne).toLowerCase(); return "*" === e ? function() { return !0 } : function(e) { return e.nodeName && e.nodeName.toLowerCase() === t } },
                        CLASS: function(e) { var t = E[e + " "]; return t || (t = new RegExp("(^|" + R + ")" + e + "(" + R + "|$)")) && E(e, (function(e) { return t.test("string" == typeof e.className && e.className || void 0 !== e.getAttribute && e.getAttribute("class") || "") })) },
                        ATTR: function(e, t, n) { return function(r) { var o = ae.attr(r, e); return null == o ? "!=" === t : !t || (o += "", "=" === t ? o === n : "!=" === t ? o !== n : "^=" === t ? n && 0 === o.indexOf(n) : "*=" === t ? n && -1 < o.indexOf(n) : "$=" === t ? n && o.slice(-n.length) === n : "~=" === t ? -1 < (" " + o.replace(F, " ") + " ").indexOf(n) : "|=" === t && (o === n || o.slice(0, n.length + 1) === n + "-")) } },
                        CHILD: function(e, t, n, r, o) {
                            var i = "nth" !== e.slice(0, 3),
                                s = "last" !== e.slice(-4),
                                a = "of-type" === t;
                            return 1 === r && 0 === o ? function(e) { return !!e.parentNode } : function(t, n, c) {
                                var u, l, p, f, d, h, y = i !== s ? "nextSibling" : "previousSibling",
                                    v = t.parentNode,
                                    g = a && t.nodeName.toLowerCase(),
                                    m = !c && !a,
                                    b = !1;
                                if (v) {
                                    if (i) {
                                        for (; y;) {
                                            for (f = t; f = f[y];)
                                                if (a ? f.nodeName.toLowerCase() === g : 1 === f.nodeType) return !1;
                                            h = y = "only" === e && !h && "nextSibling"
                                        }
                                        return !0
                                    }
                                    if (h = [s ? v.firstChild : v.lastChild], s && m) {
                                        for (b = (d = (u = (l = (p = (f = v)[w] || (f[w] = {}))[f.uniqueID] || (p[f.uniqueID] = {}))[e] || [])[0] === T && u[1]) && u[2], f = d && v.childNodes[d]; f = ++d && f && f[y] || (b = d = 0) || h.pop();)
                                            if (1 === f.nodeType && ++b && f === t) { l[e] = [T, d, b]; break }
                                    } else if (m && (b = d = (u = (l = (p = (f = t)[w] || (f[w] = {}))[f.uniqueID] || (p[f.uniqueID] = {}))[e] || [])[0] === T && u[1]), !1 === b)
                                        for (;
                                            (f = ++d && f && f[y] || (b = d = 0) || h.pop()) && ((a ? f.nodeName.toLowerCase() !== g : 1 !== f.nodeType) || !++b || (m && ((l = (p = f[w] || (f[w] = {}))[f.uniqueID] || (p[f.uniqueID] = {}))[e] = [T, b]), f !== t)););
                                    return (b -= o) === r || b % r == 0 && 0 <= b / r
                                }
                            }
                        },
                        PSEUDO: function(e, t) { var n, o = r.pseudos[e] || r.setFilters[e.toLowerCase()] || ae.error("unsupported pseudo: " + e); return o[w] ? o(t) : 1 < o.length ? (n = [e, e, "", t], r.setFilters.hasOwnProperty(e.toLowerCase()) ? ue((function(e, n) { for (var r, i = o(e, t), s = i.length; s--;) e[r = P(e, i[s])] = !(n[r] = i[s]) })) : function(e) { return o(e, 0, n) }) : o }
                    },
                    pseudos: {
                        not: ue((function(e) {
                            var t = [],
                                n = [],
                                r = a(e.replace(B, "$1"));
                            return r[w] ? ue((function(e, t, n, o) { for (var i, s = r(e, null, o, []), a = e.length; a--;)(i = s[a]) && (e[a] = !(t[a] = i)) })) : function(e, o, i) { return t[0] = e, r(t, null, i, n), t[0] = null, !n.pop() }
                        })),
                        has: ue((function(e) { return function(t) { return 0 < ae(e, t).length } })),
                        contains: ue((function(e) {
                            return e = e.replace(te, ne),
                                function(t) { return -1 < (t.textContent || o(t)).indexOf(e) }
                        })),
                        lang: ue((function(e) {
                            return G.test(e || "") || ae.error("unsupported lang: " + e), e = e.replace(te, ne).toLowerCase(),
                                function(t) {
                                    var n;
                                    do { if (n = y ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return (n = n.toLowerCase()) === e || 0 === n.indexOf(e + "-") } while ((t = t.parentNode) && 1 === t.nodeType);
                                    return !1
                                }
                        })),
                        target: function(t) { var n = e.location && e.location.hash; return n && n.slice(1) === t.id },
                        root: function(e) { return e === h },
                        focus: function(e) { return e === d.activeElement && (!d.hasFocus || d.hasFocus()) && !!(e.type || e.href || ~e.tabIndex) },
                        enabled: ye(!1),
                        disabled: ye(!0),
                        checked: function(e) { var t = e.nodeName.toLowerCase(); return "input" === t && !!e.checked || "option" === t && !!e.selected },
                        selected: function(e) { return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected },
                        empty: function(e) {
                            for (e = e.firstChild; e; e = e.nextSibling)
                                if (e.nodeType < 6) return !1;
                            return !0
                        },
                        parent: function(e) { return !r.pseudos.empty(e) },
                        header: function(e) { return Q.test(e.nodeName) },
                        input: function(e) { return J.test(e.nodeName) },
                        button: function(e) { var t = e.nodeName.toLowerCase(); return "input" === t && "button" === e.type || "button" === t },
                        text: function(e) { var t; return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase()) },
                        first: ve((function() { return [0] })),
                        last: ve((function(e, t) { return [t - 1] })),
                        eq: ve((function(e, t, n) { return [n < 0 ? n + t : n] })),
                        even: ve((function(e, t) { for (var n = 0; n < t; n += 2) e.push(n); return e })),
                        odd: ve((function(e, t) { for (var n = 1; n < t; n += 2) e.push(n); return e })),
                        lt: ve((function(e, t, n) { for (var r = n < 0 ? n + t : t < n ? t : n; 0 <= --r;) e.push(r); return e })),
                        gt: ve((function(e, t, n) { for (var r = n < 0 ? n + t : n; ++r < t;) e.push(r); return e }))
                    }
                }).pseudos.nth = r.pseudos.eq, { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }) r.pseudos[t] = de(t);
            for (t in { submit: !0, reset: !0 }) r.pseudos[t] = he(t);

            function me() {}

            function be(e) { for (var t = 0, n = e.length, r = ""; t < n; t++) r += e[t].value; return r }

            function we(e, t, n) {
                var r = t.dir,
                    o = t.next,
                    i = o || r,
                    s = n && "parentNode" === i,
                    a = S++;
                return t.first ? function(t, n, o) {
                    for (; t = t[r];)
                        if (1 === t.nodeType || s) return e(t, n, o);
                    return !1
                } : function(t, n, c) {
                    var u, l, p, f = [T, a];
                    if (c) {
                        for (; t = t[r];)
                            if ((1 === t.nodeType || s) && e(t, n, c)) return !0
                    } else
                        for (; t = t[r];)
                            if (1 === t.nodeType || s)
                                if (l = (p = t[w] || (t[w] = {}))[t.uniqueID] || (p[t.uniqueID] = {}), o && o === t.nodeName.toLowerCase()) t = t[r] || t;
                                else { if ((u = l[i]) && u[0] === T && u[1] === a) return f[2] = u[2]; if ((l[i] = f)[2] = e(t, n, c)) return !0 } return !1
                }
            }

            function xe(e) {
                return 1 < e.length ? function(t, n, r) {
                    for (var o = e.length; o--;)
                        if (!e[o](t, n, r)) return !1;
                    return !0
                } : e[0]
            }

            function Te(e, t, n, r, o) { for (var i, s = [], a = 0, c = e.length, u = null != t; a < c; a++)(i = e[a]) && (n && !n(i, r, o) || (s.push(i), u && t.push(a))); return s }

            function Se(e, t, n, r, o, i) {
                return r && !r[w] && (r = Se(r)), o && !o[w] && (o = Se(o, i)), ue((function(i, s, a, c) {
                    var u, l, p, f = [],
                        d = [],
                        h = s.length,
                        y = i || function(e, t, n) { for (var r = 0, o = t.length; r < o; r++) ae(e, t[r], n); return n }(t || "*", a.nodeType ? [a] : a, []),
                        v = !e || !i && t ? y : Te(y, f, e, a, c),
                        g = n ? o || (i ? e : h || r) ? [] : s : v;
                    if (n && n(v, g, a, c), r)
                        for (u = Te(g, d), r(u, [], a, c), l = u.length; l--;)(p = u[l]) && (g[d[l]] = !(v[d[l]] = p));
                    if (i) {
                        if (o || e) {
                            if (o) {
                                for (u = [], l = g.length; l--;)(p = g[l]) && u.push(v[l] = p);
                                o(null, g = [], u, c)
                            }
                            for (l = g.length; l--;)(p = g[l]) && -1 < (u = o ? P(i, p) : f[l]) && (i[u] = !(s[u] = p))
                        }
                    } else g = Te(g === s ? g.splice(h, g.length) : g), o ? o(null, s, g, c) : L.apply(s, g)
                }))
            }

            function Ee(e) {
                for (var t, n, o, i = e.length, s = r.relative[e[0].type], a = s || r.relative[" "], c = s ? 1 : 0, l = we((function(e) { return e === t }), a, !0), p = we((function(e) { return -1 < P(t, e) }), a, !0), f = [function(e, n, r) { var o = !s && (r || n !== u) || ((t = n).nodeType ? l(e, n, r) : p(e, n, r)); return t = null, o }]; c < i; c++)
                    if (n = r.relative[e[c].type]) f = [we(xe(f), n)];
                    else {
                        if ((n = r.filter[e[c].type].apply(null, e[c].matches))[w]) { for (o = ++c; o < i && !r.relative[e[o].type]; o++); return Se(1 < c && xe(f), 1 < c && be(e.slice(0, c - 1).concat({ value: " " === e[c - 2].type ? "*" : "" })).replace(B, "$1"), n, c < o && Ee(e.slice(c, o)), o < i && Ee(e = e.slice(o)), o < i && be(e)) }
                        f.push(n)
                    }
                return xe(f)
            }
            return me.prototype = r.filters = r.pseudos, r.setFilters = new me, s = ae.tokenize = function(e, t) { var n, o, i, s, a, c, u, l = C[e + " "]; if (l) return t ? 0 : l.slice(0); for (a = e, c = [], u = r.preFilter; a;) { for (s in n && !(o = $.exec(a)) || (o && (a = a.slice(o[0].length) || a), c.push(i = [])), n = !1, (o = U.exec(a)) && (n = o.shift(), i.push({ value: n, type: o[0].replace(B, " ") }), a = a.slice(n.length)), r.filter) !(o = X[s].exec(a)) || u[s] && !(o = u[s](o)) || (n = o.shift(), i.push({ value: n, type: s, matches: o }), a = a.slice(n.length)); if (!n) break } return t ? a.length : a ? ae.error(e) : C(e, c).slice(0) }, a = ae.compile = function(e, t) {
                var n, o, i, a, c, l, p = [],
                    h = [],
                    v = k[e + " "];
                if (!v) {
                    for (t || (t = s(e)), n = t.length; n--;)(v = Ee(t[n]))[w] ? p.push(v) : h.push(v);
                    (v = k(e, (o = h, a = 0 < (i = p).length, c = 0 < o.length, l = function(e, t, n, s, l) {
                        var p, h, v, g = 0,
                            m = "0",
                            b = e && [],
                            w = [],
                            x = u,
                            S = e || c && r.find.TAG("*", l),
                            E = T += null == x ? 1 : Math.random() || .1,
                            C = S.length;
                        for (l && (u = t == d || t || l); m !== C && null != (p = S[m]); m++) {
                            if (c && p) {
                                for (h = 0, t || p.ownerDocument == d || (f(p), n = !y); v = o[h++];)
                                    if (v(p, t || d, n)) { s.push(p); break }
                                l && (T = E)
                            }
                            a && ((p = !v && p) && g--, e && b.push(p))
                        }
                        if (g += m, a && m !== g) {
                            for (h = 0; v = i[h++];) v(b, w, t, n);
                            if (e) {
                                if (0 < g)
                                    for (; m--;) b[m] || w[m] || (w[m] = D.call(s));
                                w = Te(w)
                            }
                            L.apply(s, w), l && !e && 0 < w.length && 1 < g + i.length && ae.uniqueSort(s)
                        }
                        return l && (T = E, u = x), b
                    }, a ? ue(l) : l))).selector = e
                }
                return v
            }, c = ae.select = function(e, t, n, o) {
                var i, c, u, l, p, f = "function" == typeof e && e,
                    d = !o && s(e = f.selector || e);
                if (n = n || [], 1 === d.length) {
                    if (2 < (c = d[0] = d[0].slice(0)).length && "ID" === (u = c[0]).type && 9 === t.nodeType && y && r.relative[c[1].type]) {
                        if (!(t = (r.find.ID(u.matches[0].replace(te, ne), t) || [])[0])) return n;
                        f && (t = t.parentNode), e = e.slice(c.shift().value.length)
                    }
                    for (i = X.needsContext.test(e) ? 0 : c.length; i-- && (u = c[i], !r.relative[l = u.type]);)
                        if ((p = r.find[l]) && (o = p(u.matches[0].replace(te, ne), ee.test(c[0].type) && ge(t.parentNode) || t))) { if (c.splice(i, 1), !(e = o.length && be(c))) return L.apply(n, o), n; break }
                }
                return (f || a(e, d))(o, t, !y, n, !t || ee.test(e) && ge(t.parentNode) || t), n
            }, n.sortStable = w.split("").sort(A).join("") === w, n.detectDuplicates = !!p, f(), n.sortDetached = le((function(e) { return 1 & e.compareDocumentPosition(d.createElement("fieldset")) })), le((function(e) { return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href") })) || pe("type|href|height|width", (function(e, t, n) { if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2) })), n.attributes && le((function(e) { return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value") })) || pe("value", (function(e, t, n) { if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue })), le((function(e) { return null == e.getAttribute("disabled") })) || pe(I, (function(e, t, n) { var r; if (!n) return !0 === e[t] ? t.toLowerCase() : (r = e.getAttributeNode(t)) && r.specified ? r.value : null })), ae
        }(n);
        E.find = k, E.expr = k.selectors, E.expr[":"] = E.expr.pseudos, E.uniqueSort = E.unique = k.uniqueSort, E.text = k.getText, E.isXMLDoc = k.isXML, E.contains = k.contains, E.escapeSelector = k.escape;
        var j = function(e, t, n) {
                for (var r = [], o = void 0 !== n;
                    (e = e[t]) && 9 !== e.nodeType;)
                    if (1 === e.nodeType) {
                        if (o && E(e).is(n)) break;
                        r.push(e)
                    }
                return r
            },
            A = function(e, t) { for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e); return n },
            N = E.expr.match.needsContext;

        function O(e, t) { return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase() }
        var D = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

        function _(e, t, n) { return g(t) ? E.grep(e, (function(e, r) { return !!t.call(e, r, e) !== n })) : t.nodeType ? E.grep(e, (function(e) { return e === t !== n })) : "string" != typeof t ? E.grep(e, (function(e) { return -1 < l.call(t, e) !== n })) : E.filter(t, e, n) }
        E.filter = function(e, t, n) { var r = t[0]; return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === r.nodeType ? E.find.matchesSelector(r, e) ? [r] : [] : E.find.matches(e, E.grep(t, (function(e) { return 1 === e.nodeType }))) }, E.fn.extend({
            find: function(e) {
                var t, n, r = this.length,
                    o = this;
                if ("string" != typeof e) return this.pushStack(E(e).filter((function() {
                    for (t = 0; t < r; t++)
                        if (E.contains(o[t], this)) return !0
                })));
                for (n = this.pushStack([]), t = 0; t < r; t++) E.find(e, o[t], n);
                return 1 < r ? E.uniqueSort(n) : n
            },
            filter: function(e) { return this.pushStack(_(this, e || [], !1)) },
            not: function(e) { return this.pushStack(_(this, e || [], !0)) },
            is: function(e) { return !!_(this, "string" == typeof e && N.test(e) ? E(e) : e || [], !1).length }
        });
        var L, q = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
        (E.fn.init = function(e, t, n) {
            var r, o;
            if (!e) return this;
            if (n = n || L, "string" == typeof e) {
                if (!(r = "<" === e[0] && ">" === e[e.length - 1] && 3 <= e.length ? [null, e, null] : q.exec(e)) || !r[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
                if (r[1]) {
                    if (t = t instanceof E ? t[0] : t, E.merge(this, E.parseHTML(r[1], t && t.nodeType ? t.ownerDocument || t : b, !0)), D.test(r[1]) && E.isPlainObject(t))
                        for (r in t) g(this[r]) ? this[r](t[r]) : this.attr(r, t[r]);
                    return this
                }
                return (o = b.getElementById(r[2])) && (this[0] = o, this.length = 1), this
            }
            return e.nodeType ? (this[0] = e, this.length = 1, this) : g(e) ? void 0 !== n.ready ? n.ready(e) : e(E) : E.makeArray(e, this)
        }).prototype = E.fn, L = E(b);
        var P = /^(?:parents|prev(?:Until|All))/,
            I = { children: !0, contents: !0, next: !0, prev: !0 };

        function R(e, t) {
            for (;
                (e = e[t]) && 1 !== e.nodeType;);
            return e
        }
        E.fn.extend({
            has: function(e) {
                var t = E(e, this),
                    n = t.length;
                return this.filter((function() {
                    for (var e = 0; e < n; e++)
                        if (E.contains(this, t[e])) return !0
                }))
            },
            closest: function(e, t) {
                var n, r = 0,
                    o = this.length,
                    i = [],
                    s = "string" != typeof e && E(e);
                if (!N.test(e))
                    for (; r < o; r++)
                        for (n = this[r]; n && n !== t; n = n.parentNode)
                            if (n.nodeType < 11 && (s ? -1 < s.index(n) : 1 === n.nodeType && E.find.matchesSelector(n, e))) { i.push(n); break }
                return this.pushStack(1 < i.length ? E.uniqueSort(i) : i)
            },
            index: function(e) { return e ? "string" == typeof e ? l.call(E(e), this[0]) : l.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1 },
            add: function(e, t) { return this.pushStack(E.uniqueSort(E.merge(this.get(), E(e, t)))) },
            addBack: function(e) { return this.add(null == e ? this.prevObject : this.prevObject.filter(e)) }
        }), E.each({ parent: function(e) { var t = e.parentNode; return t && 11 !== t.nodeType ? t : null }, parents: function(e) { return j(e, "parentNode") }, parentsUntil: function(e, t, n) { return j(e, "parentNode", n) }, next: function(e) { return R(e, "nextSibling") }, prev: function(e) { return R(e, "previousSibling") }, nextAll: function(e) { return j(e, "nextSibling") }, prevAll: function(e) { return j(e, "previousSibling") }, nextUntil: function(e, t, n) { return j(e, "nextSibling", n) }, prevUntil: function(e, t, n) { return j(e, "previousSibling", n) }, siblings: function(e) { return A((e.parentNode || {}).firstChild, e) }, children: function(e) { return A(e.firstChild) }, contents: function(e) { return null != e.contentDocument && s(e.contentDocument) ? e.contentDocument : (O(e, "template") && (e = e.content || e), E.merge([], e.childNodes)) } }, (function(e, t) { E.fn[e] = function(n, r) { var o = E.map(this, t, n); return "Until" !== e.slice(-5) && (r = n), r && "string" == typeof r && (o = E.filter(r, o)), 1 < this.length && (I[e] || E.uniqueSort(o), P.test(e) && o.reverse()), this.pushStack(o) } }));
        var H = /[^\x20\t\r\n\f]+/g;

        function W(e) { return e }

        function M(e) { throw e }

        function F(e, t, n, r) { var o; try { e && g(o = e.promise) ? o.call(e).done(t).fail(n) : e && g(o = e.then) ? o.call(e, t, n) : t.apply(void 0, [e].slice(r)) } catch (e) { n.apply(void 0, [e]) } }
        E.Callbacks = function(e) {
            var t, n;
            e = "string" == typeof e ? (t = e, n = {}, E.each(t.match(H) || [], (function(e, t) { n[t] = !0 })), n) : E.extend({}, e);
            var r, o, i, s, a = [],
                c = [],
                u = -1,
                l = function() {
                    for (s = s || e.once, i = r = !0; c.length; u = -1)
                        for (o = c.shift(); ++u < a.length;) !1 === a[u].apply(o[0], o[1]) && e.stopOnFalse && (u = a.length, o = !1);
                    e.memory || (o = !1), r = !1, s && (a = o ? [] : "")
                },
                p = { add: function() { return a && (o && !r && (u = a.length - 1, c.push(o)), function t(n) { E.each(n, (function(n, r) { g(r) ? e.unique && p.has(r) || a.push(r) : r && r.length && "string" !== T(r) && t(r) })) }(arguments), o && !r && l()), this }, remove: function() { return E.each(arguments, (function(e, t) { for (var n; - 1 < (n = E.inArray(t, a, n));) a.splice(n, 1), n <= u && u-- })), this }, has: function(e) { return e ? -1 < E.inArray(e, a) : 0 < a.length }, empty: function() { return a && (a = []), this }, disable: function() { return s = c = [], a = o = "", this }, disabled: function() { return !a }, lock: function() { return s = c = [], o || r || (a = o = ""), this }, locked: function() { return !!s }, fireWith: function(e, t) { return s || (t = [e, (t = t || []).slice ? t.slice() : t], c.push(t), r || l()), this }, fire: function() { return p.fireWith(this, arguments), this }, fired: function() { return !!i } };
            return p
        }, E.extend({
            Deferred: function(e) {
                var t = [
                        ["notify", "progress", E.Callbacks("memory"), E.Callbacks("memory"), 2],
                        ["resolve", "done", E.Callbacks("once memory"), E.Callbacks("once memory"), 0, "resolved"],
                        ["reject", "fail", E.Callbacks("once memory"), E.Callbacks("once memory"), 1, "rejected"]
                    ],
                    r = "pending",
                    o = {
                        state: function() { return r },
                        always: function() { return i.done(arguments).fail(arguments), this },
                        catch: function(e) { return o.then(null, e) },
                        pipe: function() {
                            var e = arguments;
                            return E.Deferred((function(n) {
                                E.each(t, (function(t, r) {
                                    var o = g(e[r[4]]) && e[r[4]];
                                    i[r[1]]((function() {
                                        var e = o && o.apply(this, arguments);
                                        e && g(e.promise) ? e.promise().progress(n.notify).done(n.resolve).fail(n.reject) : n[r[0] + "With"](this, o ? [e] : arguments)
                                    }))
                                })), e = null
                            })).promise()
                        },
                        then: function(e, r, o) {
                            var i = 0;

                            function s(e, t, r, o) {
                                return function() {
                                    var a = this,
                                        c = arguments,
                                        u = function() {
                                            var n, u;
                                            if (!(e < i)) {
                                                if ((n = r.apply(a, c)) === t.promise()) throw new TypeError("Thenable self-resolution");
                                                u = n && ("object" == typeof n || "function" == typeof n) && n.then, g(u) ? o ? u.call(n, s(i, t, W, o), s(i, t, M, o)) : (i++, u.call(n, s(i, t, W, o), s(i, t, M, o), s(i, t, W, t.notifyWith))) : (r !== W && (a = void 0, c = [n]), (o || t.resolveWith)(a, c))
                                            }
                                        },
                                        l = o ? u : function() { try { u() } catch (n) { E.Deferred.exceptionHook && E.Deferred.exceptionHook(n, l.stackTrace), i <= e + 1 && (r !== M && (a = void 0, c = [n]), t.rejectWith(a, c)) } };
                                    e ? l() : (E.Deferred.getStackHook && (l.stackTrace = E.Deferred.getStackHook()), n.setTimeout(l))
                                }
                            }
                            return E.Deferred((function(n) { t[0][3].add(s(0, n, g(o) ? o : W, n.notifyWith)), t[1][3].add(s(0, n, g(e) ? e : W)), t[2][3].add(s(0, n, g(r) ? r : M)) })).promise()
                        },
                        promise: function(e) { return null != e ? E.extend(e, o) : o }
                    },
                    i = {};
                return E.each(t, (function(e, n) {
                    var s = n[2],
                        a = n[5];
                    o[n[1]] = s.add, a && s.add((function() { r = a }), t[3 - e][2].disable, t[3 - e][3].disable, t[0][2].lock, t[0][3].lock), s.add(n[3].fire), i[n[0]] = function() { return i[n[0] + "With"](this === i ? void 0 : this, arguments), this }, i[n[0] + "With"] = s.fireWith
                })), o.promise(i), e && e.call(i, i), i
            },
            when: function(e) {
                var t = arguments.length,
                    n = t,
                    r = Array(n),
                    o = a.call(arguments),
                    i = E.Deferred(),
                    s = function(e) { return function(n) { r[e] = this, o[e] = 1 < arguments.length ? a.call(arguments) : n, --t || i.resolveWith(r, o) } };
                if (t <= 1 && (F(e, i.done(s(n)).resolve, i.reject, !t), "pending" === i.state() || g(o[n] && o[n].then))) return i.then();
                for (; n--;) F(o[n], s(n), i.reject);
                return i.promise()
            }
        });
        var B = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
        E.Deferred.exceptionHook = function(e, t) { n.console && n.console.warn && e && B.test(e.name) && n.console.warn("jQuery.Deferred exception: " + e.message, e.stack, t) }, E.readyException = function(e) { n.setTimeout((function() { throw e })) };
        var $ = E.Deferred();

        function U() { b.removeEventListener("DOMContentLoaded", U), n.removeEventListener("load", U), E.ready() }
        E.fn.ready = function(e) { return $.then(e).catch((function(e) { E.readyException(e) })), this }, E.extend({
            isReady: !1,
            readyWait: 1,
            ready: function(e) {
                (!0 === e ? --E.readyWait : E.isReady) || (E.isReady = !0) !== e && 0 < --E.readyWait || $.resolveWith(b, [E])
            }
        }), E.ready.then = $.then, "complete" === b.readyState || "loading" !== b.readyState && !b.documentElement.doScroll ? n.setTimeout(E.ready) : (b.addEventListener("DOMContentLoaded", U), n.addEventListener("load", U));
        var z = function(e, t, n, r, o, i, s) {
                var a = 0,
                    c = e.length,
                    u = null == n;
                if ("object" === T(n))
                    for (a in o = !0, n) z(e, t, a, n[a], !0, i, s);
                else if (void 0 !== r && (o = !0, g(r) || (s = !0), u && (s ? (t.call(e, r), t = null) : (u = t, t = function(e, t, n) { return u.call(E(e), n) })), t))
                    for (; a < c; a++) t(e[a], n, s ? r : r.call(e[a], a, t(e[a], n)));
                return o ? e : u ? t.call(e) : c ? t(e[0], n) : i
            },
            Y = /^-ms-/,
            G = /-([a-z])/g;

        function X(e, t) { return t.toUpperCase() }

        function V(e) { return e.replace(Y, "ms-").replace(G, X) }
        var J = function(e) { return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType };

        function Q() { this.expando = E.expando + Q.uid++ }
        Q.uid = 1, Q.prototype = {
            cache: function(e) { var t = e[this.expando]; return t || (t = {}, J(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, { value: t, configurable: !0 }))), t },
            set: function(e, t, n) {
                var r, o = this.cache(e);
                if ("string" == typeof t) o[V(t)] = n;
                else
                    for (r in t) o[V(r)] = t[r];
                return o
            },
            get: function(e, t) { return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][V(t)] },
            access: function(e, t, n) { return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t) },
            remove: function(e, t) { var n, r = e[this.expando]; if (void 0 !== r) { if (void 0 !== t) { n = (t = Array.isArray(t) ? t.map(V) : (t = V(t)) in r ? [t] : t.match(H) || []).length; for (; n--;) delete r[t[n]] }(void 0 === t || E.isEmptyObject(r)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando]) } },
            hasData: function(e) { var t = e[this.expando]; return void 0 !== t && !E.isEmptyObject(t) }
        };
        var K = new Q,
            Z = new Q,
            ee = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
            te = /[A-Z]/g;

        function ne(e, t, n) {
            var r, o;
            if (void 0 === n && 1 === e.nodeType)
                if (r = "data-" + t.replace(te, "-$&").toLowerCase(), "string" == typeof(n = e.getAttribute(r))) {
                    try { n = "true" === (o = n) || "false" !== o && ("null" === o ? null : o === +o + "" ? +o : ee.test(o) ? JSON.parse(o) : o) } catch (e) {}
                    Z.set(e, t, n)
                } else n = void 0;
            return n
        }
        E.extend({ hasData: function(e) { return Z.hasData(e) || K.hasData(e) }, data: function(e, t, n) { return Z.access(e, t, n) }, removeData: function(e, t) { Z.remove(e, t) }, _data: function(e, t, n) { return K.access(e, t, n) }, _removeData: function(e, t) { K.remove(e, t) } }), E.fn.extend({
            data: function(e, t) {
                var n, r, o, i = this[0],
                    s = i && i.attributes;
                if (void 0 === e) {
                    if (this.length && (o = Z.get(i), 1 === i.nodeType && !K.get(i, "hasDataAttrs"))) {
                        for (n = s.length; n--;) s[n] && 0 === (r = s[n].name).indexOf("data-") && (r = V(r.slice(5)), ne(i, r, o[r]));
                        K.set(i, "hasDataAttrs", !0)
                    }
                    return o
                }
                return "object" == typeof e ? this.each((function() { Z.set(this, e) })) : z(this, (function(t) {
                    var n;
                    if (i && void 0 === t) return void 0 !== (n = Z.get(i, e)) || void 0 !== (n = ne(i, e)) ? n : void 0;
                    this.each((function() { Z.set(this, e, t) }))
                }), null, t, 1 < arguments.length, null, !0)
            },
            removeData: function(e) { return this.each((function() { Z.remove(this, e) })) }
        }), E.extend({
            queue: function(e, t, n) { var r; if (e) return t = (t || "fx") + "queue", r = K.get(e, t), n && (!r || Array.isArray(n) ? r = K.access(e, t, E.makeArray(n)) : r.push(n)), r || [] },
            dequeue: function(e, t) {
                t = t || "fx";
                var n = E.queue(e, t),
                    r = n.length,
                    o = n.shift(),
                    i = E._queueHooks(e, t);
                "inprogress" === o && (o = n.shift(), r--), o && ("fx" === t && n.unshift("inprogress"), delete i.stop, o.call(e, (function() { E.dequeue(e, t) }), i)), !r && i && i.empty.fire()
            },
            _queueHooks: function(e, t) { var n = t + "queueHooks"; return K.get(e, n) || K.access(e, n, { empty: E.Callbacks("once memory").add((function() { K.remove(e, [t + "queue", n]) })) }) }
        }), E.fn.extend({
            queue: function(e, t) {
                var n = 2;
                return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? E.queue(this[0], e) : void 0 === t ? this : this.each((function() {
                    var n = E.queue(this, e, t);
                    E._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && E.dequeue(this, e)
                }))
            },
            dequeue: function(e) { return this.each((function() { E.dequeue(this, e) })) },
            clearQueue: function(e) { return this.queue(e || "fx", []) },
            promise: function(e, t) {
                var n, r = 1,
                    o = E.Deferred(),
                    i = this,
                    s = this.length,
                    a = function() {--r || o.resolveWith(i, [i]) };
                for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; s--;)(n = K.get(i[s], e + "queueHooks")) && n.empty && (r++, n.empty.add(a));
                return a(), o.promise(t)
            }
        });
        var re = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
            oe = new RegExp("^(?:([+-])=|)(" + re + ")([a-z%]*)$", "i"),
            ie = ["Top", "Right", "Bottom", "Left"],
            se = b.documentElement,
            ae = function(e) { return E.contains(e.ownerDocument, e) },
            ce = { composed: !0 };
        se.getRootNode && (ae = function(e) { return E.contains(e.ownerDocument, e) || e.getRootNode(ce) === e.ownerDocument });
        var ue = function(e, t) { return "none" === (e = t || e).style.display || "" === e.style.display && ae(e) && "none" === E.css(e, "display") };

        function le(e, t, n, r) {
            var o, i, s = 20,
                a = r ? function() { return r.cur() } : function() { return E.css(e, t, "") },
                c = a(),
                u = n && n[3] || (E.cssNumber[t] ? "" : "px"),
                l = e.nodeType && (E.cssNumber[t] || "px" !== u && +c) && oe.exec(E.css(e, t));
            if (l && l[3] !== u) {
                for (c /= 2, u = u || l[3], l = +c || 1; s--;) E.style(e, t, l + u), (1 - i) * (1 - (i = a() / c || .5)) <= 0 && (s = 0), l /= i;
                l *= 2, E.style(e, t, l + u), n = n || []
            }
            return n && (l = +l || +c || 0, o = n[1] ? l + (n[1] + 1) * n[2] : +n[2], r && (r.unit = u, r.start = l, r.end = o)), o
        }
        var pe = {};

        function fe(e, t) { for (var n, r, o, i, s, a, c, u = [], l = 0, p = e.length; l < p; l++)(r = e[l]).style && (n = r.style.display, t ? ("none" === n && (u[l] = K.get(r, "display") || null, u[l] || (r.style.display = "")), "" === r.style.display && ue(r) && (u[l] = (c = s = i = void 0, s = (o = r).ownerDocument, a = o.nodeName, (c = pe[a]) || (i = s.body.appendChild(s.createElement(a)), c = E.css(i, "display"), i.parentNode.removeChild(i), "none" === c && (c = "block"), pe[a] = c)))) : "none" !== n && (u[l] = "none", K.set(r, "display", n))); for (l = 0; l < p; l++) null != u[l] && (e[l].style.display = u[l]); return e }
        E.fn.extend({ show: function() { return fe(this, !0) }, hide: function() { return fe(this) }, toggle: function(e) { return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each((function() { ue(this) ? E(this).show() : E(this).hide() })) } });
        var de, he, ye = /^(?:checkbox|radio)$/i,
            ve = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i,
            ge = /^$|^module$|\/(?:java|ecma)script/i;
        de = b.createDocumentFragment().appendChild(b.createElement("div")), (he = b.createElement("input")).setAttribute("type", "radio"), he.setAttribute("checked", "checked"), he.setAttribute("name", "t"), de.appendChild(he), v.checkClone = de.cloneNode(!0).cloneNode(!0).lastChild.checked, de.innerHTML = "<textarea>x</textarea>", v.noCloneChecked = !!de.cloneNode(!0).lastChild.defaultValue, de.innerHTML = "<option></option>", v.option = !!de.lastChild;
        var me = { thead: [1, "<table>", "</table>"], col: [2, "<table><colgroup>", "</colgroup></table>"], tr: [2, "<table><tbody>", "</tbody></table>"], td: [3, "<table><tbody><tr>", "</tr></tbody></table>"], _default: [0, "", ""] };

        function be(e, t) { var n; return n = void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t || "*") : void 0 !== e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && O(e, t) ? E.merge([e], n) : n }

        function we(e, t) { for (var n = 0, r = e.length; n < r; n++) K.set(e[n], "globalEval", !t || K.get(t[n], "globalEval")) }
        me.tbody = me.tfoot = me.colgroup = me.caption = me.thead, me.th = me.td, v.option || (me.optgroup = me.option = [1, "<select multiple='multiple'>", "</select>"]);
        var xe = /<|&#?\w+;/;

        function Te(e, t, n, r, o) {
            for (var i, s, a, c, u, l, p = t.createDocumentFragment(), f = [], d = 0, h = e.length; d < h; d++)
                if ((i = e[d]) || 0 === i)
                    if ("object" === T(i)) E.merge(f, i.nodeType ? [i] : i);
                    else if (xe.test(i)) {
                for (s = s || p.appendChild(t.createElement("div")), a = (ve.exec(i) || ["", ""])[1].toLowerCase(), c = me[a] || me._default, s.innerHTML = c[1] + E.htmlPrefilter(i) + c[2], l = c[0]; l--;) s = s.lastChild;
                E.merge(f, s.childNodes), (s = p.firstChild).textContent = ""
            } else f.push(t.createTextNode(i));
            for (p.textContent = "", d = 0; i = f[d++];)
                if (r && -1 < E.inArray(i, r)) o && o.push(i);
                else if (u = ae(i), s = be(p.appendChild(i), "script"), u && we(s), n)
                for (l = 0; i = s[l++];) ge.test(i.type || "") && n.push(i);
            return p
        }
        var Se = /^key/,
            Ee = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
            Ce = /^([^.]*)(?:\.(.+)|)/;

        function ke() { return !0 }

        function je() { return !1 }

        function Ae(e, t) { return e === function() { try { return b.activeElement } catch (e) {} }() == ("focus" === t) }

        function Ne(e, t, n, r, o, i) {
            var s, a;
            if ("object" == typeof t) { for (a in "string" != typeof n && (r = r || n, n = void 0), t) Ne(e, a, n, r, t[a], i); return e }
            if (null == r && null == o ? (o = n, r = n = void 0) : null == o && ("string" == typeof n ? (o = r, r = void 0) : (o = r, r = n, n = void 0)), !1 === o) o = je;
            else if (!o) return e;
            return 1 === i && (s = o, (o = function(e) { return E().off(e), s.apply(this, arguments) }).guid = s.guid || (s.guid = E.guid++)), e.each((function() { E.event.add(this, t, o, r, n) }))
        }

        function Oe(e, t, n) {
            n ? (K.set(e, t, !1), E.event.add(e, t, {
                namespace: !1,
                handler: function(e) {
                    var r, o, i = K.get(this, t);
                    if (1 & e.isTrigger && this[t]) {
                        if (i.length)(E.event.special[t] || {}).delegateType && e.stopPropagation();
                        else if (i = a.call(arguments), K.set(this, t, i), r = n(this, t), this[t](), i !== (o = K.get(this, t)) || r ? K.set(this, t, !1) : o = {}, i !== o) return e.stopImmediatePropagation(), e.preventDefault(), o.value
                    } else i.length && (K.set(this, t, { value: E.event.trigger(E.extend(i[0], E.Event.prototype), i.slice(1), this) }), e.stopImmediatePropagation())
                }
            })) : void 0 === K.get(e, t) && E.event.add(e, t, ke)
        }
        E.event = {
            global: {},
            add: function(e, t, n, r, o) {
                var i, s, a, c, u, l, p, f, d, h, y, v = K.get(e);
                if (J(e))
                    for (n.handler && (n = (i = n).handler, o = i.selector), o && E.find.matchesSelector(se, o), n.guid || (n.guid = E.guid++), (c = v.events) || (c = v.events = Object.create(null)), (s = v.handle) || (s = v.handle = function(t) { return void 0 !== E && E.event.triggered !== t.type ? E.event.dispatch.apply(e, arguments) : void 0 }), u = (t = (t || "").match(H) || [""]).length; u--;) d = y = (a = Ce.exec(t[u]) || [])[1], h = (a[2] || "").split(".").sort(), d && (p = E.event.special[d] || {}, d = (o ? p.delegateType : p.bindType) || d, p = E.event.special[d] || {}, l = E.extend({ type: d, origType: y, data: r, handler: n, guid: n.guid, selector: o, needsContext: o && E.expr.match.needsContext.test(o), namespace: h.join(".") }, i), (f = c[d]) || ((f = c[d] = []).delegateCount = 0, p.setup && !1 !== p.setup.call(e, r, h, s) || e.addEventListener && e.addEventListener(d, s)), p.add && (p.add.call(e, l), l.handler.guid || (l.handler.guid = n.guid)), o ? f.splice(f.delegateCount++, 0, l) : f.push(l), E.event.global[d] = !0)
            },
            remove: function(e, t, n, r, o) {
                var i, s, a, c, u, l, p, f, d, h, y, v = K.hasData(e) && K.get(e);
                if (v && (c = v.events)) {
                    for (u = (t = (t || "").match(H) || [""]).length; u--;)
                        if (d = y = (a = Ce.exec(t[u]) || [])[1], h = (a[2] || "").split(".").sort(), d) {
                            for (p = E.event.special[d] || {}, f = c[d = (r ? p.delegateType : p.bindType) || d] || [], a = a[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = i = f.length; i--;) l = f[i], !o && y !== l.origType || n && n.guid !== l.guid || a && !a.test(l.namespace) || r && r !== l.selector && ("**" !== r || !l.selector) || (f.splice(i, 1), l.selector && f.delegateCount--, p.remove && p.remove.call(e, l));
                            s && !f.length && (p.teardown && !1 !== p.teardown.call(e, h, v.handle) || E.removeEvent(e, d, v.handle), delete c[d])
                        } else
                            for (d in c) E.event.remove(e, d + t[u], n, r, !0);
                    E.isEmptyObject(c) && K.remove(e, "handle events")
                }
            },
            dispatch: function(e) {
                var t, n, r, o, i, s, a = new Array(arguments.length),
                    c = E.event.fix(e),
                    u = (K.get(this, "events") || Object.create(null))[c.type] || [],
                    l = E.event.special[c.type] || {};
                for (a[0] = c, t = 1; t < arguments.length; t++) a[t] = arguments[t];
                if (c.delegateTarget = this, !l.preDispatch || !1 !== l.preDispatch.call(this, c)) {
                    for (s = E.event.handlers.call(this, c, u), t = 0;
                        (o = s[t++]) && !c.isPropagationStopped();)
                        for (c.currentTarget = o.elem, n = 0;
                            (i = o.handlers[n++]) && !c.isImmediatePropagationStopped();) c.rnamespace && !1 !== i.namespace && !c.rnamespace.test(i.namespace) || (c.handleObj = i, c.data = i.data, void 0 !== (r = ((E.event.special[i.origType] || {}).handle || i.handler).apply(o.elem, a)) && !1 === (c.result = r) && (c.preventDefault(), c.stopPropagation()));
                    return l.postDispatch && l.postDispatch.call(this, c), c.result
                }
            },
            handlers: function(e, t) {
                var n, r, o, i, s, a = [],
                    c = t.delegateCount,
                    u = e.target;
                if (c && u.nodeType && !("click" === e.type && 1 <= e.button))
                    for (; u !== this; u = u.parentNode || this)
                        if (1 === u.nodeType && ("click" !== e.type || !0 !== u.disabled)) {
                            for (i = [], s = {}, n = 0; n < c; n++) void 0 === s[o = (r = t[n]).selector + " "] && (s[o] = r.needsContext ? -1 < E(o, this).index(u) : E.find(o, this, null, [u]).length), s[o] && i.push(r);
                            i.length && a.push({ elem: u, handlers: i })
                        }
                return u = this, c < t.length && a.push({ elem: u, handlers: t.slice(c) }), a
            },
            addProp: function(e, t) { Object.defineProperty(E.Event.prototype, e, { enumerable: !0, configurable: !0, get: g(t) ? function() { if (this.originalEvent) return t(this.originalEvent) } : function() { if (this.originalEvent) return this.originalEvent[e] }, set: function(t) { Object.defineProperty(this, e, { enumerable: !0, configurable: !0, writable: !0, value: t }) } }) },
            fix: function(e) { return e[E.expando] ? e : new E.Event(e) },
            special: { load: { noBubble: !0 }, click: { setup: function(e) { var t = this || e; return ye.test(t.type) && t.click && O(t, "input") && Oe(t, "click", ke), !1 }, trigger: function(e) { var t = this || e; return ye.test(t.type) && t.click && O(t, "input") && Oe(t, "click"), !0 }, _default: function(e) { var t = e.target; return ye.test(t.type) && t.click && O(t, "input") && K.get(t, "click") || O(t, "a") } }, beforeunload: { postDispatch: function(e) { void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result) } } }
        }, E.removeEvent = function(e, t, n) { e.removeEventListener && e.removeEventListener(t, n) }, E.Event = function(e, t) {
            if (!(this instanceof E.Event)) return new E.Event(e, t);
            e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? ke : je, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && E.extend(this, t), this.timeStamp = e && e.timeStamp || Date.now(), this[E.expando] = !0
        }, E.Event.prototype = {
            constructor: E.Event,
            isDefaultPrevented: je,
            isPropagationStopped: je,
            isImmediatePropagationStopped: je,
            isSimulated: !1,
            preventDefault: function() {
                var e = this.originalEvent;
                this.isDefaultPrevented = ke, e && !this.isSimulated && e.preventDefault()
            },
            stopPropagation: function() {
                var e = this.originalEvent;
                this.isPropagationStopped = ke, e && !this.isSimulated && e.stopPropagation()
            },
            stopImmediatePropagation: function() {
                var e = this.originalEvent;
                this.isImmediatePropagationStopped = ke, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
            }
        }, E.each({ altKey: !0, bubbles: !0, cancelable: !0, changedTouches: !0, ctrlKey: !0, detail: !0, eventPhase: !0, metaKey: !0, pageX: !0, pageY: !0, shiftKey: !0, view: !0, char: !0, code: !0, charCode: !0, key: !0, keyCode: !0, button: !0, buttons: !0, clientX: !0, clientY: !0, offsetX: !0, offsetY: !0, pointerId: !0, pointerType: !0, screenX: !0, screenY: !0, targetTouches: !0, toElement: !0, touches: !0, which: function(e) { var t = e.button; return null == e.which && Se.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && Ee.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which } }, E.event.addProp), E.each({ focus: "focusin", blur: "focusout" }, (function(e, t) { E.event.special[e] = { setup: function() { return Oe(this, e, Ae), !1 }, trigger: function() { return Oe(this, e), !0 }, delegateType: t } })), E.each({ mouseenter: "mouseover", mouseleave: "mouseout", pointerenter: "pointerover", pointerleave: "pointerout" }, (function(e, t) {
            E.event.special[e] = {
                delegateType: t,
                bindType: t,
                handle: function(e) {
                    var n, r = e.relatedTarget,
                        o = e.handleObj;
                    return r && (r === this || E.contains(this, r)) || (e.type = o.origType, n = o.handler.apply(this, arguments), e.type = t), n
                }
            }
        })), E.fn.extend({ on: function(e, t, n, r) { return Ne(this, e, t, n, r) }, one: function(e, t, n, r) { return Ne(this, e, t, n, r, 1) }, off: function(e, t, n) { var r, o; if (e && e.preventDefault && e.handleObj) return r = e.handleObj, E(e.delegateTarget).off(r.namespace ? r.origType + "." + r.namespace : r.origType, r.selector, r.handler), this; if ("object" == typeof e) { for (o in e) this.off(o, t, e[o]); return this } return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = je), this.each((function() { E.event.remove(this, e, n, t) })) } });
        var De = /<script|<style|<link/i,
            _e = /checked\s*(?:[^=]|=\s*.checked.)/i,
            Le = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

        function qe(e, t) { return O(e, "table") && O(11 !== t.nodeType ? t : t.firstChild, "tr") && E(e).children("tbody")[0] || e }

        function Pe(e) { return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e }

        function Ie(e) { return "true/" === (e.type || "").slice(0, 5) ? e.type = e.type.slice(5) : e.removeAttribute("type"), e }

        function Re(e, t) {
            var n, r, o, i, s, a;
            if (1 === t.nodeType) {
                if (K.hasData(e) && (a = K.get(e).events))
                    for (o in K.remove(t, "handle events"), a)
                        for (n = 0, r = a[o].length; n < r; n++) E.event.add(t, o, a[o][n]);
                Z.hasData(e) && (i = Z.access(e), s = E.extend({}, i), Z.set(t, s))
            }
        }

        function He(e, t, n, r) {
            t = c(t);
            var o, i, s, a, u, l, p = 0,
                f = e.length,
                d = f - 1,
                h = t[0],
                y = g(h);
            if (y || 1 < f && "string" == typeof h && !v.checkClone && _e.test(h)) return e.each((function(o) {
                var i = e.eq(o);
                y && (t[0] = h.call(this, o, i.html())), He(i, t, n, r)
            }));
            if (f && (i = (o = Te(t, e[0].ownerDocument, !1, e, r)).firstChild, 1 === o.childNodes.length && (o = i), i || r)) {
                for (a = (s = E.map(be(o, "script"), Pe)).length; p < f; p++) u = o, p !== d && (u = E.clone(u, !0, !0), a && E.merge(s, be(u, "script"))), n.call(e[p], u, p);
                if (a)
                    for (l = s[s.length - 1].ownerDocument, E.map(s, Ie), p = 0; p < a; p++) u = s[p], ge.test(u.type || "") && !K.access(u, "globalEval") && E.contains(l, u) && (u.src && "module" !== (u.type || "").toLowerCase() ? E._evalUrl && !u.noModule && E._evalUrl(u.src, { nonce: u.nonce || u.getAttribute("nonce") }, l) : x(u.textContent.replace(Le, ""), u, l))
            }
            return e
        }

        function We(e, t, n) { for (var r, o = t ? E.filter(t, e) : e, i = 0; null != (r = o[i]); i++) n || 1 !== r.nodeType || E.cleanData(be(r)), r.parentNode && (n && ae(r) && we(be(r, "script")), r.parentNode.removeChild(r)); return e }
        E.extend({
            htmlPrefilter: function(e) { return e },
            clone: function(e, t, n) {
                var r, o, i, s, a, c, u, l = e.cloneNode(!0),
                    p = ae(e);
                if (!(v.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || E.isXMLDoc(e)))
                    for (s = be(l), r = 0, o = (i = be(e)).length; r < o; r++) a = i[r], "input" === (u = (c = s[r]).nodeName.toLowerCase()) && ye.test(a.type) ? c.checked = a.checked : "input" !== u && "textarea" !== u || (c.defaultValue = a.defaultValue);
                if (t)
                    if (n)
                        for (i = i || be(e), s = s || be(l), r = 0, o = i.length; r < o; r++) Re(i[r], s[r]);
                    else Re(e, l);
                return 0 < (s = be(l, "script")).length && we(s, !p && be(e, "script")), l
            },
            cleanData: function(e) {
                for (var t, n, r, o = E.event.special, i = 0; void 0 !== (n = e[i]); i++)
                    if (J(n)) {
                        if (t = n[K.expando]) {
                            if (t.events)
                                for (r in t.events) o[r] ? E.event.remove(n, r) : E.removeEvent(n, r, t.handle);
                            n[K.expando] = void 0
                        }
                        n[Z.expando] && (n[Z.expando] = void 0)
                    }
            }
        }), E.fn.extend({
            detach: function(e) { return We(this, e, !0) },
            remove: function(e) { return We(this, e) },
            text: function(e) { return z(this, (function(e) { return void 0 === e ? E.text(this) : this.empty().each((function() { 1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e) })) }), null, e, arguments.length) },
            append: function() { return He(this, arguments, (function(e) { 1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || qe(this, e).appendChild(e) })) },
            prepend: function() {
                return He(this, arguments, (function(e) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var t = qe(this, e);
                        t.insertBefore(e, t.firstChild)
                    }
                }))
            },
            before: function() { return He(this, arguments, (function(e) { this.parentNode && this.parentNode.insertBefore(e, this) })) },
            after: function() { return He(this, arguments, (function(e) { this.parentNode && this.parentNode.insertBefore(e, this.nextSibling) })) },
            empty: function() { for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (E.cleanData(be(e, !1)), e.textContent = ""); return this },
            clone: function(e, t) { return e = null != e && e, t = null == t ? e : t, this.map((function() { return E.clone(this, e, t) })) },
            html: function(e) {
                return z(this, (function(e) {
                    var t = this[0] || {},
                        n = 0,
                        r = this.length;
                    if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                    if ("string" == typeof e && !De.test(e) && !me[(ve.exec(e) || ["", ""])[1].toLowerCase()]) {
                        e = E.htmlPrefilter(e);
                        try {
                            for (; n < r; n++) 1 === (t = this[n] || {}).nodeType && (E.cleanData(be(t, !1)), t.innerHTML = e);
                            t = 0
                        } catch (e) {}
                    }
                    t && this.empty().append(e)
                }), null, e, arguments.length)
            },
            replaceWith: function() {
                var e = [];
                return He(this, arguments, (function(t) {
                    var n = this.parentNode;
                    E.inArray(this, e) < 0 && (E.cleanData(be(this)), n && n.replaceChild(t, this))
                }), e)
            }
        }), E.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, (function(e, t) { E.fn[e] = function(e) { for (var n, r = [], o = E(e), i = o.length - 1, s = 0; s <= i; s++) n = s === i ? this : this.clone(!0), E(o[s])[t](n), u.apply(r, n.get()); return this.pushStack(r) } }));
        var Me = new RegExp("^(" + re + ")(?!px)[a-z%]+$", "i"),
            Fe = function(e) { var t = e.ownerDocument.defaultView; return t && t.opener || (t = n), t.getComputedStyle(e) },
            Be = function(e, t, n) { var r, o, i = {}; for (o in t) i[o] = e.style[o], e.style[o] = t[o]; for (o in r = n.call(e), t) e.style[o] = i[o]; return r },
            $e = new RegExp(ie.join("|"), "i");

        function Ue(e, t, n) { var r, o, i, s, a = e.style; return (n = n || Fe(e)) && ("" !== (s = n.getPropertyValue(t) || n[t]) || ae(e) || (s = E.style(e, t)), !v.pixelBoxStyles() && Me.test(s) && $e.test(t) && (r = a.width, o = a.minWidth, i = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = r, a.minWidth = o, a.maxWidth = i)), void 0 !== s ? s + "" : s }

        function ze(e, t) {
            return {
                get: function() {
                    if (!e()) return (this.get = t).apply(this, arguments);
                    delete this.get
                }
            }
        }! function() {
            function e() {
                if (l) {
                    u.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", l.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", se.appendChild(u).appendChild(l);
                    var e = n.getComputedStyle(l);
                    r = "1%" !== e.top, c = 12 === t(e.marginLeft), l.style.right = "60%", s = 36 === t(e.right), o = 36 === t(e.width), l.style.position = "absolute", i = 12 === t(l.offsetWidth / 3), se.removeChild(u), l = null
                }
            }

            function t(e) { return Math.round(parseFloat(e)) }
            var r, o, i, s, a, c, u = b.createElement("div"),
                l = b.createElement("div");
            l.style && (l.style.backgroundClip = "content-box", l.cloneNode(!0).style.backgroundClip = "", v.clearCloneStyle = "content-box" === l.style.backgroundClip, E.extend(v, { boxSizingReliable: function() { return e(), o }, pixelBoxStyles: function() { return e(), s }, pixelPosition: function() { return e(), r }, reliableMarginLeft: function() { return e(), c }, scrollboxSize: function() { return e(), i }, reliableTrDimensions: function() { var e, t, r, o; return null == a && (e = b.createElement("table"), t = b.createElement("tr"), r = b.createElement("div"), e.style.cssText = "position:absolute;left:-11111px", t.style.height = "1px", r.style.height = "9px", se.appendChild(e).appendChild(t).appendChild(r), o = n.getComputedStyle(t), a = 3 < parseInt(o.height), se.removeChild(e)), a } }))
        }();
        var Ye = ["Webkit", "Moz", "ms"],
            Ge = b.createElement("div").style,
            Xe = {};

        function Ve(e) {
            return E.cssProps[e] || Xe[e] || (e in Ge ? e : Xe[e] = function(e) {
                for (var t = e[0].toUpperCase() + e.slice(1), n = Ye.length; n--;)
                    if ((e = Ye[n] + t) in Ge) return e
            }(e) || e)
        }
        var Je = /^(none|table(?!-c[ea]).+)/,
            Qe = /^--/,
            Ke = { position: "absolute", visibility: "hidden", display: "block" },
            Ze = { letterSpacing: "0", fontWeight: "400" };

        function et(e, t, n) { var r = oe.exec(t); return r ? Math.max(0, r[2] - (n || 0)) + (r[3] || "px") : t }

        function tt(e, t, n, r, o, i) {
            var s = "width" === t ? 1 : 0,
                a = 0,
                c = 0;
            if (n === (r ? "border" : "content")) return 0;
            for (; s < 4; s += 2) "margin" === n && (c += E.css(e, n + ie[s], !0, o)), r ? ("content" === n && (c -= E.css(e, "padding" + ie[s], !0, o)), "margin" !== n && (c -= E.css(e, "border" + ie[s] + "Width", !0, o))) : (c += E.css(e, "padding" + ie[s], !0, o), "padding" !== n ? c += E.css(e, "border" + ie[s] + "Width", !0, o) : a += E.css(e, "border" + ie[s] + "Width", !0, o));
            return !r && 0 <= i && (c += Math.max(0, Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - i - c - a - .5)) || 0), c
        }

        function nt(e, t, n) {
            var r = Fe(e),
                o = (!v.boxSizingReliable() || n) && "border-box" === E.css(e, "boxSizing", !1, r),
                i = o,
                s = Ue(e, t, r),
                a = "offset" + t[0].toUpperCase() + t.slice(1);
            if (Me.test(s)) {
                if (!n) return s;
                s = "auto"
            }
            return (!v.boxSizingReliable() && o || !v.reliableTrDimensions() && O(e, "tr") || "auto" === s || !parseFloat(s) && "inline" === E.css(e, "display", !1, r)) && e.getClientRects().length && (o = "border-box" === E.css(e, "boxSizing", !1, r), (i = a in e) && (s = e[a])), (s = parseFloat(s) || 0) + tt(e, t, n || (o ? "border" : "content"), i, r, s) + "px"
        }

        function rt(e, t, n, r, o) { return new rt.prototype.init(e, t, n, r, o) }
        E.extend({
            cssHooks: { opacity: { get: function(e, t) { if (t) { var n = Ue(e, "opacity"); return "" === n ? "1" : n } } } },
            cssNumber: { animationIterationCount: !0, columnCount: !0, fillOpacity: !0, flexGrow: !0, flexShrink: !0, fontWeight: !0, gridArea: !0, gridColumn: !0, gridColumnEnd: !0, gridColumnStart: !0, gridRow: !0, gridRowEnd: !0, gridRowStart: !0, lineHeight: !0, opacity: !0, order: !0, orphans: !0, widows: !0, zIndex: !0, zoom: !0 },
            cssProps: {},
            style: function(e, t, n, r) {
                if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                    var o, i, s, a = V(t),
                        c = Qe.test(t),
                        u = e.style;
                    if (c || (t = Ve(a)), s = E.cssHooks[t] || E.cssHooks[a], void 0 === n) return s && "get" in s && void 0 !== (o = s.get(e, !1, r)) ? o : u[t];
                    "string" == (i = typeof n) && (o = oe.exec(n)) && o[1] && (n = le(e, t, o), i = "number"), null != n && n == n && ("number" !== i || c || (n += o && o[3] || (E.cssNumber[a] ? "" : "px")), v.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (u[t] = "inherit"), s && "set" in s && void 0 === (n = s.set(e, n, r)) || (c ? u.setProperty(t, n) : u[t] = n))
                }
            },
            css: function(e, t, n, r) { var o, i, s, a = V(t); return Qe.test(t) || (t = Ve(a)), (s = E.cssHooks[t] || E.cssHooks[a]) && "get" in s && (o = s.get(e, !0, n)), void 0 === o && (o = Ue(e, t, r)), "normal" === o && t in Ze && (o = Ze[t]), "" === n || n ? (i = parseFloat(o), !0 === n || isFinite(i) ? i || 0 : o) : o }
        }), E.each(["height", "width"], (function(e, t) {
            E.cssHooks[t] = {
                get: function(e, n, r) { if (n) return !Je.test(E.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? nt(e, t, r) : Be(e, Ke, (function() { return nt(e, t, r) })) },
                set: function(e, n, r) {
                    var o, i = Fe(e),
                        s = !v.scrollboxSize() && "absolute" === i.position,
                        a = (s || r) && "border-box" === E.css(e, "boxSizing", !1, i),
                        c = r ? tt(e, t, r, a, i) : 0;
                    return a && s && (c -= Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - parseFloat(i[t]) - tt(e, t, "border", !1, i) - .5)), c && (o = oe.exec(n)) && "px" !== (o[3] || "px") && (e.style[t] = n, n = E.css(e, t)), et(0, n, c)
                }
            }
        })), E.cssHooks.marginLeft = ze(v.reliableMarginLeft, (function(e, t) { if (t) return (parseFloat(Ue(e, "marginLeft")) || e.getBoundingClientRect().left - Be(e, { marginLeft: 0 }, (function() { return e.getBoundingClientRect().left }))) + "px" })), E.each({ margin: "", padding: "", border: "Width" }, (function(e, t) { E.cssHooks[e + t] = { expand: function(n) { for (var r = 0, o = {}, i = "string" == typeof n ? n.split(" ") : [n]; r < 4; r++) o[e + ie[r] + t] = i[r] || i[r - 2] || i[0]; return o } }, "margin" !== e && (E.cssHooks[e + t].set = et) })), E.fn.extend({
            css: function(e, t) {
                return z(this, (function(e, t, n) {
                    var r, o, i = {},
                        s = 0;
                    if (Array.isArray(t)) { for (r = Fe(e), o = t.length; s < o; s++) i[t[s]] = E.css(e, t[s], !1, r); return i }
                    return void 0 !== n ? E.style(e, t, n) : E.css(e, t)
                }), e, t, 1 < arguments.length)
            }
        }), ((E.Tween = rt).prototype = { constructor: rt, init: function(e, t, n, r, o, i) { this.elem = e, this.prop = n, this.easing = o || E.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = i || (E.cssNumber[n] ? "" : "px") }, cur: function() { var e = rt.propHooks[this.prop]; return e && e.get ? e.get(this) : rt.propHooks._default.get(this) }, run: function(e) { var t, n = rt.propHooks[this.prop]; return this.options.duration ? this.pos = t = E.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : rt.propHooks._default.set(this), this } }).init.prototype = rt.prototype, (rt.propHooks = { _default: { get: function(e) { var t; return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = E.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0 }, set: function(e) { E.fx.step[e.prop] ? E.fx.step[e.prop](e) : 1 !== e.elem.nodeType || !E.cssHooks[e.prop] && null == e.elem.style[Ve(e.prop)] ? e.elem[e.prop] = e.now : E.style(e.elem, e.prop, e.now + e.unit) } } }).scrollTop = rt.propHooks.scrollLeft = { set: function(e) { e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now) } }, E.easing = { linear: function(e) { return e }, swing: function(e) { return .5 - Math.cos(e * Math.PI) / 2 }, _default: "swing" }, E.fx = rt.prototype.init, E.fx.step = {};
        var ot, it, st, at, ct = /^(?:toggle|show|hide)$/,
            ut = /queueHooks$/;

        function lt() { it && (!1 === b.hidden && n.requestAnimationFrame ? n.requestAnimationFrame(lt) : n.setTimeout(lt, E.fx.interval), E.fx.tick()) }

        function pt() { return n.setTimeout((function() { ot = void 0 })), ot = Date.now() }

        function ft(e, t) {
            var n, r = 0,
                o = { height: e };
            for (t = t ? 1 : 0; r < 4; r += 2 - t) o["margin" + (n = ie[r])] = o["padding" + n] = e;
            return t && (o.opacity = o.width = e), o
        }

        function dt(e, t, n) {
            for (var r, o = (ht.tweeners[t] || []).concat(ht.tweeners["*"]), i = 0, s = o.length; i < s; i++)
                if (r = o[i].call(n, t, e)) return r
        }

        function ht(e, t, n) {
            var r, o, i = 0,
                s = ht.prefilters.length,
                a = E.Deferred().always((function() { delete c.elem })),
                c = function() { if (o) return !1; for (var t = ot || pt(), n = Math.max(0, u.startTime + u.duration - t), r = 1 - (n / u.duration || 0), i = 0, s = u.tweens.length; i < s; i++) u.tweens[i].run(r); return a.notifyWith(e, [u, r, n]), r < 1 && s ? n : (s || a.notifyWith(e, [u, 1, 0]), a.resolveWith(e, [u]), !1) },
                u = a.promise({
                    elem: e,
                    props: E.extend({}, t),
                    opts: E.extend(!0, { specialEasing: {}, easing: E.easing._default }, n),
                    originalProperties: t,
                    originalOptions: n,
                    startTime: ot || pt(),
                    duration: n.duration,
                    tweens: [],
                    createTween: function(t, n) { var r = E.Tween(e, u.opts, t, n, u.opts.specialEasing[t] || u.opts.easing); return u.tweens.push(r), r },
                    stop: function(t) {
                        var n = 0,
                            r = t ? u.tweens.length : 0;
                        if (o) return this;
                        for (o = !0; n < r; n++) u.tweens[n].run(1);
                        return t ? (a.notifyWith(e, [u, 1, 0]), a.resolveWith(e, [u, t])) : a.rejectWith(e, [u, t]), this
                    }
                }),
                l = u.props;
            for (function(e, t) {
                    var n, r, o, i, s;
                    for (n in e)
                        if (o = t[r = V(n)], i = e[n], Array.isArray(i) && (o = i[1], i = e[n] = i[0]), n !== r && (e[r] = i, delete e[n]), (s = E.cssHooks[r]) && "expand" in s)
                            for (n in i = s.expand(i), delete e[r], i) n in e || (e[n] = i[n], t[n] = o);
                        else t[r] = o
                }(l, u.opts.specialEasing); i < s; i++)
                if (r = ht.prefilters[i].call(u, e, l, u.opts)) return g(r.stop) && (E._queueHooks(u.elem, u.opts.queue).stop = r.stop.bind(r)), r;
            return E.map(l, dt, u), g(u.opts.start) && u.opts.start.call(e, u), u.progress(u.opts.progress).done(u.opts.done, u.opts.complete).fail(u.opts.fail).always(u.opts.always), E.fx.timer(E.extend(c, { elem: e, anim: u, queue: u.opts.queue })), u
        }
        E.Animation = E.extend(ht, {
            tweeners: { "*": [function(e, t) { var n = this.createTween(e, t); return le(n.elem, e, oe.exec(t), n), n }] },
            tweener: function(e, t) { g(e) ? (t = e, e = ["*"]) : e = e.match(H); for (var n, r = 0, o = e.length; r < o; r++) n = e[r], ht.tweeners[n] = ht.tweeners[n] || [], ht.tweeners[n].unshift(t) },
            prefilters: [function(e, t, n) {
                var r, o, i, s, a, c, u, l, p = "width" in t || "height" in t,
                    f = this,
                    d = {},
                    h = e.style,
                    y = e.nodeType && ue(e),
                    v = K.get(e, "fxshow");
                for (r in n.queue || (null == (s = E._queueHooks(e, "fx")).unqueued && (s.unqueued = 0, a = s.empty.fire, s.empty.fire = function() { s.unqueued || a() }), s.unqueued++, f.always((function() { f.always((function() { s.unqueued--, E.queue(e, "fx").length || s.empty.fire() })) }))), t)
                    if (o = t[r], ct.test(o)) {
                        if (delete t[r], i = i || "toggle" === o, o === (y ? "hide" : "show")) {
                            if ("show" !== o || !v || void 0 === v[r]) continue;
                            y = !0
                        }
                        d[r] = v && v[r] || E.style(e, r)
                    }
                if ((c = !E.isEmptyObject(t)) || !E.isEmptyObject(d))
                    for (r in p && 1 === e.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY], null == (u = v && v.display) && (u = K.get(e, "display")), "none" === (l = E.css(e, "display")) && (u ? l = u : (fe([e], !0), u = e.style.display || u, l = E.css(e, "display"), fe([e]))), ("inline" === l || "inline-block" === l && null != u) && "none" === E.css(e, "float") && (c || (f.done((function() { h.display = u })), null == u && (l = h.display, u = "none" === l ? "" : l)), h.display = "inline-block")), n.overflow && (h.overflow = "hidden", f.always((function() { h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2] }))), c = !1, d) c || (v ? "hidden" in v && (y = v.hidden) : v = K.access(e, "fxshow", { display: u }), i && (v.hidden = !y), y && fe([e], !0), f.done((function() { for (r in y || fe([e]), K.remove(e, "fxshow"), d) E.style(e, r, d[r]) }))), c = dt(y ? v[r] : 0, r, f), r in v || (v[r] = c.start, y && (c.end = c.start, c.start = 0))
            }],
            prefilter: function(e, t) { t ? ht.prefilters.unshift(e) : ht.prefilters.push(e) }
        }), E.speed = function(e, t, n) { var r = e && "object" == typeof e ? E.extend({}, e) : { complete: n || !n && t || g(e) && e, duration: e, easing: n && t || t && !g(t) && t }; return E.fx.off ? r.duration = 0 : "number" != typeof r.duration && (r.duration in E.fx.speeds ? r.duration = E.fx.speeds[r.duration] : r.duration = E.fx.speeds._default), null != r.queue && !0 !== r.queue || (r.queue = "fx"), r.old = r.complete, r.complete = function() { g(r.old) && r.old.call(this), r.queue && E.dequeue(this, r.queue) }, r }, E.fn.extend({
            fadeTo: function(e, t, n, r) { return this.filter(ue).css("opacity", 0).show().end().animate({ opacity: t }, e, n, r) },
            animate: function(e, t, n, r) {
                var o = E.isEmptyObject(e),
                    i = E.speed(t, n, r),
                    s = function() {
                        var t = ht(this, E.extend({}, e), i);
                        (o || K.get(this, "finish")) && t.stop(!0)
                    };
                return s.finish = s, o || !1 === i.queue ? this.each(s) : this.queue(i.queue, s)
            },
            stop: function(e, t, n) {
                var r = function(e) {
                    var t = e.stop;
                    delete e.stop, t(n)
                };
                return "string" != typeof e && (n = t, t = e, e = void 0), t && this.queue(e || "fx", []), this.each((function() {
                    var t = !0,
                        o = null != e && e + "queueHooks",
                        i = E.timers,
                        s = K.get(this);
                    if (o) s[o] && s[o].stop && r(s[o]);
                    else
                        for (o in s) s[o] && s[o].stop && ut.test(o) && r(s[o]);
                    for (o = i.length; o--;) i[o].elem !== this || null != e && i[o].queue !== e || (i[o].anim.stop(n), t = !1, i.splice(o, 1));
                    !t && n || E.dequeue(this, e)
                }))
            },
            finish: function(e) {
                return !1 !== e && (e = e || "fx"), this.each((function() {
                    var t, n = K.get(this),
                        r = n[e + "queue"],
                        o = n[e + "queueHooks"],
                        i = E.timers,
                        s = r ? r.length : 0;
                    for (n.finish = !0, E.queue(this, e, []), o && o.stop && o.stop.call(this, !0), t = i.length; t--;) i[t].elem === this && i[t].queue === e && (i[t].anim.stop(!0), i.splice(t, 1));
                    for (t = 0; t < s; t++) r[t] && r[t].finish && r[t].finish.call(this);
                    delete n.finish
                }))
            }
        }), E.each(["toggle", "show", "hide"], (function(e, t) {
            var n = E.fn[t];
            E.fn[t] = function(e, r, o) { return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(ft(t, !0), e, r, o) }
        })), E.each({ slideDown: ft("show"), slideUp: ft("hide"), slideToggle: ft("toggle"), fadeIn: { opacity: "show" }, fadeOut: { opacity: "hide" }, fadeToggle: { opacity: "toggle" } }, (function(e, t) { E.fn[e] = function(e, n, r) { return this.animate(t, e, n, r) } })), E.timers = [], E.fx.tick = function() {
            var e, t = 0,
                n = E.timers;
            for (ot = Date.now(); t < n.length; t++)(e = n[t])() || n[t] !== e || n.splice(t--, 1);
            n.length || E.fx.stop(), ot = void 0
        }, E.fx.timer = function(e) { E.timers.push(e), E.fx.start() }, E.fx.interval = 13, E.fx.start = function() { it || (it = !0, lt()) }, E.fx.stop = function() { it = null }, E.fx.speeds = { slow: 600, fast: 200, _default: 400 }, E.fn.delay = function(e, t) {
            return e = E.fx && E.fx.speeds[e] || e, t = t || "fx", this.queue(t, (function(t, r) {
                var o = n.setTimeout(t, e);
                r.stop = function() { n.clearTimeout(o) }
            }))
        }, st = b.createElement("input"), at = b.createElement("select").appendChild(b.createElement("option")), st.type = "checkbox", v.checkOn = "" !== st.value, v.optSelected = at.selected, (st = b.createElement("input")).value = "t", st.type = "radio", v.radioValue = "t" === st.value;
        var yt, vt = E.expr.attrHandle;
        E.fn.extend({ attr: function(e, t) { return z(this, E.attr, e, t, 1 < arguments.length) }, removeAttr: function(e) { return this.each((function() { E.removeAttr(this, e) })) } }), E.extend({
            attr: function(e, t, n) { var r, o, i = e.nodeType; if (3 !== i && 8 !== i && 2 !== i) return void 0 === e.getAttribute ? E.prop(e, t, n) : (1 === i && E.isXMLDoc(e) || (o = E.attrHooks[t.toLowerCase()] || (E.expr.match.bool.test(t) ? yt : void 0)), void 0 !== n ? null === n ? void E.removeAttr(e, t) : o && "set" in o && void 0 !== (r = o.set(e, n, t)) ? r : (e.setAttribute(t, n + ""), n) : o && "get" in o && null !== (r = o.get(e, t)) ? r : null == (r = E.find.attr(e, t)) ? void 0 : r) },
            attrHooks: { type: { set: function(e, t) { if (!v.radioValue && "radio" === t && O(e, "input")) { var n = e.value; return e.setAttribute("type", t), n && (e.value = n), t } } } },
            removeAttr: function(e, t) {
                var n, r = 0,
                    o = t && t.match(H);
                if (o && 1 === e.nodeType)
                    for (; n = o[r++];) e.removeAttribute(n)
            }
        }), yt = { set: function(e, t, n) { return !1 === t ? E.removeAttr(e, n) : e.setAttribute(n, n), n } }, E.each(E.expr.match.bool.source.match(/\w+/g), (function(e, t) {
            var n = vt[t] || E.find.attr;
            vt[t] = function(e, t, r) { var o, i, s = t.toLowerCase(); return r || (i = vt[s], vt[s] = o, o = null != n(e, t, r) ? s : null, vt[s] = i), o }
        }));
        var gt = /^(?:input|select|textarea|button)$/i,
            mt = /^(?:a|area)$/i;

        function bt(e) { return (e.match(H) || []).join(" ") }

        function wt(e) { return e.getAttribute && e.getAttribute("class") || "" }

        function xt(e) { return Array.isArray(e) ? e : "string" == typeof e && e.match(H) || [] }
        E.fn.extend({ prop: function(e, t) { return z(this, E.prop, e, t, 1 < arguments.length) }, removeProp: function(e) { return this.each((function() { delete this[E.propFix[e] || e] })) } }), E.extend({ prop: function(e, t, n) { var r, o, i = e.nodeType; if (3 !== i && 8 !== i && 2 !== i) return 1 === i && E.isXMLDoc(e) || (t = E.propFix[t] || t, o = E.propHooks[t]), void 0 !== n ? o && "set" in o && void 0 !== (r = o.set(e, n, t)) ? r : e[t] = n : o && "get" in o && null !== (r = o.get(e, t)) ? r : e[t] }, propHooks: { tabIndex: { get: function(e) { var t = E.find.attr(e, "tabindex"); return t ? parseInt(t, 10) : gt.test(e.nodeName) || mt.test(e.nodeName) && e.href ? 0 : -1 } } }, propFix: { for: "htmlFor", class: "className" } }), v.optSelected || (E.propHooks.selected = {
            get: function(e) { var t = e.parentNode; return t && t.parentNode && t.parentNode.selectedIndex, null },
            set: function(e) {
                var t = e.parentNode;
                t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
            }
        }), E.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], (function() { E.propFix[this.toLowerCase()] = this })), E.fn.extend({
            addClass: function(e) {
                var t, n, r, o, i, s, a, c = 0;
                if (g(e)) return this.each((function(t) { E(this).addClass(e.call(this, t, wt(this))) }));
                if ((t = xt(e)).length)
                    for (; n = this[c++];)
                        if (o = wt(n), r = 1 === n.nodeType && " " + bt(o) + " ") {
                            for (s = 0; i = t[s++];) r.indexOf(" " + i + " ") < 0 && (r += i + " ");
                            o !== (a = bt(r)) && n.setAttribute("class", a)
                        }
                return this
            },
            removeClass: function(e) {
                var t, n, r, o, i, s, a, c = 0;
                if (g(e)) return this.each((function(t) { E(this).removeClass(e.call(this, t, wt(this))) }));
                if (!arguments.length) return this.attr("class", "");
                if ((t = xt(e)).length)
                    for (; n = this[c++];)
                        if (o = wt(n), r = 1 === n.nodeType && " " + bt(o) + " ") {
                            for (s = 0; i = t[s++];)
                                for (; - 1 < r.indexOf(" " + i + " ");) r = r.replace(" " + i + " ", " ");
                            o !== (a = bt(r)) && n.setAttribute("class", a)
                        }
                return this
            },
            toggleClass: function(e, t) {
                var n = typeof e,
                    r = "string" === n || Array.isArray(e);
                return "boolean" == typeof t && r ? t ? this.addClass(e) : this.removeClass(e) : g(e) ? this.each((function(n) { E(this).toggleClass(e.call(this, n, wt(this), t), t) })) : this.each((function() {
                    var t, o, i, s;
                    if (r)
                        for (o = 0, i = E(this), s = xt(e); t = s[o++];) i.hasClass(t) ? i.removeClass(t) : i.addClass(t);
                    else void 0 !== e && "boolean" !== n || ((t = wt(this)) && K.set(this, "__className__", t), this.setAttribute && this.setAttribute("class", t || !1 === e ? "" : K.get(this, "__className__") || ""))
                }))
            },
            hasClass: function(e) {
                var t, n, r = 0;
                for (t = " " + e + " "; n = this[r++];)
                    if (1 === n.nodeType && -1 < (" " + bt(wt(n)) + " ").indexOf(t)) return !0;
                return !1
            }
        });
        var Tt = /\r/g;
        E.fn.extend({
            val: function(e) {
                var t, n, r, o = this[0];
                return arguments.length ? (r = g(e), this.each((function(n) {
                    var o;
                    1 === this.nodeType && (null == (o = r ? e.call(this, n, E(this).val()) : e) ? o = "" : "number" == typeof o ? o += "" : Array.isArray(o) && (o = E.map(o, (function(e) { return null == e ? "" : e + "" }))), (t = E.valHooks[this.type] || E.valHooks[this.nodeName.toLowerCase()]) && "set" in t && void 0 !== t.set(this, o, "value") || (this.value = o))
                }))) : o ? (t = E.valHooks[o.type] || E.valHooks[o.nodeName.toLowerCase()]) && "get" in t && void 0 !== (n = t.get(o, "value")) ? n : "string" == typeof(n = o.value) ? n.replace(Tt, "") : null == n ? "" : n : void 0
            }
        }), E.extend({
            valHooks: {
                option: { get: function(e) { var t = E.find.attr(e, "value"); return null != t ? t : bt(E.text(e)) } },
                select: {
                    get: function(e) {
                        var t, n, r, o = e.options,
                            i = e.selectedIndex,
                            s = "select-one" === e.type,
                            a = s ? null : [],
                            c = s ? i + 1 : o.length;
                        for (r = i < 0 ? c : s ? i : 0; r < c; r++)
                            if (((n = o[r]).selected || r === i) && !n.disabled && (!n.parentNode.disabled || !O(n.parentNode, "optgroup"))) {
                                if (t = E(n).val(), s) return t;
                                a.push(t)
                            }
                        return a
                    },
                    set: function(e, t) { for (var n, r, o = e.options, i = E.makeArray(t), s = o.length; s--;)((r = o[s]).selected = -1 < E.inArray(E.valHooks.option.get(r), i)) && (n = !0); return n || (e.selectedIndex = -1), i }
                }
            }
        }), E.each(["radio", "checkbox"], (function() { E.valHooks[this] = { set: function(e, t) { if (Array.isArray(t)) return e.checked = -1 < E.inArray(E(e).val(), t) } }, v.checkOn || (E.valHooks[this].get = function(e) { return null === e.getAttribute("value") ? "on" : e.value }) })), v.focusin = "onfocusin" in n;
        var St = /^(?:focusinfocus|focusoutblur)$/,
            Et = function(e) { e.stopPropagation() };
        E.extend(E.event, {
            trigger: function(e, t, r, o) {
                var i, s, a, c, u, l, p, f, h = [r || b],
                    y = d.call(e, "type") ? e.type : e,
                    v = d.call(e, "namespace") ? e.namespace.split(".") : [];
                if (s = f = a = r = r || b, 3 !== r.nodeType && 8 !== r.nodeType && !St.test(y + E.event.triggered) && (-1 < y.indexOf(".") && (y = (v = y.split(".")).shift(), v.sort()), u = y.indexOf(":") < 0 && "on" + y, (e = e[E.expando] ? e : new E.Event(y, "object" == typeof e && e)).isTrigger = o ? 2 : 3, e.namespace = v.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + v.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = r), t = null == t ? [e] : E.makeArray(t, [e]), p = E.event.special[y] || {}, o || !p.trigger || !1 !== p.trigger.apply(r, t))) {
                    if (!o && !p.noBubble && !m(r)) {
                        for (c = p.delegateType || y, St.test(c + y) || (s = s.parentNode); s; s = s.parentNode) h.push(s), a = s;
                        a === (r.ownerDocument || b) && h.push(a.defaultView || a.parentWindow || n)
                    }
                    for (i = 0;
                        (s = h[i++]) && !e.isPropagationStopped();) f = s, e.type = 1 < i ? c : p.bindType || y, (l = (K.get(s, "events") || Object.create(null))[e.type] && K.get(s, "handle")) && l.apply(s, t), (l = u && s[u]) && l.apply && J(s) && (e.result = l.apply(s, t), !1 === e.result && e.preventDefault());
                    return e.type = y, o || e.isDefaultPrevented() || p._default && !1 !== p._default.apply(h.pop(), t) || !J(r) || u && g(r[y]) && !m(r) && ((a = r[u]) && (r[u] = null), E.event.triggered = y, e.isPropagationStopped() && f.addEventListener(y, Et), r[y](), e.isPropagationStopped() && f.removeEventListener(y, Et), E.event.triggered = void 0, a && (r[u] = a)), e.result
                }
            },
            simulate: function(e, t, n) {
                var r = E.extend(new E.Event, n, { type: e, isSimulated: !0 });
                E.event.trigger(r, null, t)
            }
        }), E.fn.extend({ trigger: function(e, t) { return this.each((function() { E.event.trigger(e, t, this) })) }, triggerHandler: function(e, t) { var n = this[0]; if (n) return E.event.trigger(e, t, n, !0) } }), v.focusin || E.each({ focus: "focusin", blur: "focusout" }, (function(e, t) {
            var n = function(e) { E.event.simulate(t, e.target, E.event.fix(e)) };
            E.event.special[t] = {
                setup: function() {
                    var r = this.ownerDocument || this.document || this,
                        o = K.access(r, t);
                    o || r.addEventListener(e, n, !0), K.access(r, t, (o || 0) + 1)
                },
                teardown: function() {
                    var r = this.ownerDocument || this.document || this,
                        o = K.access(r, t) - 1;
                    o ? K.access(r, t, o) : (r.removeEventListener(e, n, !0), K.remove(r, t))
                }
            }
        }));
        var Ct = n.location,
            kt = { guid: Date.now() },
            jt = /\?/;
        E.parseXML = function(e) { var t; if (!e || "string" != typeof e) return null; try { t = (new n.DOMParser).parseFromString(e, "text/xml") } catch (e) { t = void 0 } return t && !t.getElementsByTagName("parsererror").length || E.error("Invalid XML: " + e), t };
        var At = /\[\]$/,
            Nt = /\r?\n/g,
            Ot = /^(?:submit|button|image|reset|file)$/i,
            Dt = /^(?:input|select|textarea|keygen)/i;

        function _t(e, t, n, r) {
            var o;
            if (Array.isArray(t)) E.each(t, (function(t, o) { n || At.test(e) ? r(e, o) : _t(e + "[" + ("object" == typeof o && null != o ? t : "") + "]", o, n, r) }));
            else if (n || "object" !== T(t)) r(e, t);
            else
                for (o in t) _t(e + "[" + o + "]", t[o], n, r)
        }
        E.param = function(e, t) {
            var n, r = [],
                o = function(e, t) {
                    var n = g(t) ? t() : t;
                    r[r.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
                };
            if (null == e) return "";
            if (Array.isArray(e) || e.jquery && !E.isPlainObject(e)) E.each(e, (function() { o(this.name, this.value) }));
            else
                for (n in e) _t(n, e[n], t, o);
            return r.join("&")
        }, E.fn.extend({ serialize: function() { return E.param(this.serializeArray()) }, serializeArray: function() { return this.map((function() { var e = E.prop(this, "elements"); return e ? E.makeArray(e) : this })).filter((function() { var e = this.type; return this.name && !E(this).is(":disabled") && Dt.test(this.nodeName) && !Ot.test(e) && (this.checked || !ye.test(e)) })).map((function(e, t) { var n = E(this).val(); return null == n ? null : Array.isArray(n) ? E.map(n, (function(e) { return { name: t.name, value: e.replace(Nt, "\r\n") } })) : { name: t.name, value: n.replace(Nt, "\r\n") } })).get() } });
        var Lt = /%20/g,
            qt = /#.*$/,
            Pt = /([?&])_=[^&]*/,
            It = /^(.*?):[ \t]*([^\r\n]*)$/gm,
            Rt = /^(?:GET|HEAD)$/,
            Ht = /^\/\//,
            Wt = {},
            Mt = {},
            Ft = "*/".concat("*"),
            Bt = b.createElement("a");

        function $t(e) {
            return function(t, n) {
                "string" != typeof t && (n = t, t = "*");
                var r, o = 0,
                    i = t.toLowerCase().match(H) || [];
                if (g(n))
                    for (; r = i[o++];) "+" === r[0] ? (r = r.slice(1) || "*", (e[r] = e[r] || []).unshift(n)) : (e[r] = e[r] || []).push(n)
            }
        }

        function Ut(e, t, n, r) {
            var o = {},
                i = e === Mt;

            function s(a) { var c; return o[a] = !0, E.each(e[a] || [], (function(e, a) { var u = a(t, n, r); return "string" != typeof u || i || o[u] ? i ? !(c = u) : void 0 : (t.dataTypes.unshift(u), s(u), !1) })), c }
            return s(t.dataTypes[0]) || !o["*"] && s("*")
        }

        function zt(e, t) { var n, r, o = E.ajaxSettings.flatOptions || {}; for (n in t) void 0 !== t[n] && ((o[n] ? e : r || (r = {}))[n] = t[n]); return r && E.extend(!0, e, r), e }
        Bt.href = Ct.href, E.extend({
            active: 0,
            lastModified: {},
            etag: {},
            ajaxSettings: { url: Ct.href, type: "GET", isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Ct.protocol), global: !0, processData: !0, async: !0, contentType: "application/x-www-form-urlencoded; charset=UTF-8", accepts: { "*": Ft, text: "text/plain", html: "text/html", xml: "application/xml, text/xml", json: "application/json, text/javascript" }, contents: { xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/ }, responseFields: { xml: "responseXML", text: "responseText", json: "responseJSON" }, converters: { "* text": String, "text html": !0, "text json": JSON.parse, "text xml": E.parseXML }, flatOptions: { url: !0, context: !0 } },
            ajaxSetup: function(e, t) { return t ? zt(zt(e, E.ajaxSettings), t) : zt(E.ajaxSettings, e) },
            ajaxPrefilter: $t(Wt),
            ajaxTransport: $t(Mt),
            ajax: function(e, t) {
                "object" == typeof e && (t = e, e = void 0), t = t || {};
                var r, o, i, s, a, c, u, l, p, f, d = E.ajaxSetup({}, t),
                    h = d.context || d,
                    y = d.context && (h.nodeType || h.jquery) ? E(h) : E.event,
                    v = E.Deferred(),
                    g = E.Callbacks("once memory"),
                    m = d.statusCode || {},
                    w = {},
                    x = {},
                    T = "canceled",
                    S = {
                        readyState: 0,
                        getResponseHeader: function(e) {
                            var t;
                            if (u) {
                                if (!s)
                                    for (s = {}; t = It.exec(i);) s[t[1].toLowerCase() + " "] = (s[t[1].toLowerCase() + " "] || []).concat(t[2]);
                                t = s[e.toLowerCase() + " "]
                            }
                            return null == t ? null : t.join(", ")
                        },
                        getAllResponseHeaders: function() { return u ? i : null },
                        setRequestHeader: function(e, t) { return null == u && (e = x[e.toLowerCase()] = x[e.toLowerCase()] || e, w[e] = t), this },
                        overrideMimeType: function(e) { return null == u && (d.mimeType = e), this },
                        statusCode: function(e) {
                            var t;
                            if (e)
                                if (u) S.always(e[S.status]);
                                else
                                    for (t in e) m[t] = [m[t], e[t]];
                            return this
                        },
                        abort: function(e) { var t = e || T; return r && r.abort(t), C(0, t), this }
                    };
                if (v.promise(S), d.url = ((e || d.url || Ct.href) + "").replace(Ht, Ct.protocol + "//"), d.type = t.method || t.type || d.method || d.type, d.dataTypes = (d.dataType || "*").toLowerCase().match(H) || [""], null == d.crossDomain) { c = b.createElement("a"); try { c.href = d.url, c.href = c.href, d.crossDomain = Bt.protocol + "//" + Bt.host != c.protocol + "//" + c.host } catch (e) { d.crossDomain = !0 } }
                if (d.data && d.processData && "string" != typeof d.data && (d.data = E.param(d.data, d.traditional)), Ut(Wt, d, t, S), u) return S;
                for (p in (l = E.event && d.global) && 0 == E.active++ && E.event.trigger("ajaxStart"), d.type = d.type.toUpperCase(), d.hasContent = !Rt.test(d.type), o = d.url.replace(qt, ""), d.hasContent ? d.data && d.processData && 0 === (d.contentType || "").indexOf("application/x-www-form-urlencoded") && (d.data = d.data.replace(Lt, "+")) : (f = d.url.slice(o.length), d.data && (d.processData || "string" == typeof d.data) && (o += (jt.test(o) ? "&" : "?") + d.data, delete d.data), !1 === d.cache && (o = o.replace(Pt, "$1"), f = (jt.test(o) ? "&" : "?") + "_=" + kt.guid++ + f), d.url = o + f), d.ifModified && (E.lastModified[o] && S.setRequestHeader("If-Modified-Since", E.lastModified[o]), E.etag[o] && S.setRequestHeader("If-None-Match", E.etag[o])), (d.data && d.hasContent && !1 !== d.contentType || t.contentType) && S.setRequestHeader("Content-Type", d.contentType), S.setRequestHeader("Accept", d.dataTypes[0] && d.accepts[d.dataTypes[0]] ? d.accepts[d.dataTypes[0]] + ("*" !== d.dataTypes[0] ? ", " + Ft + "; q=0.01" : "") : d.accepts["*"]), d.headers) S.setRequestHeader(p, d.headers[p]);
                if (d.beforeSend && (!1 === d.beforeSend.call(h, S, d) || u)) return S.abort();
                if (T = "abort", g.add(d.complete), S.done(d.success), S.fail(d.error), r = Ut(Mt, d, t, S)) {
                    if (S.readyState = 1, l && y.trigger("ajaxSend", [S, d]), u) return S;
                    d.async && 0 < d.timeout && (a = n.setTimeout((function() { S.abort("timeout") }), d.timeout));
                    try { u = !1, r.send(w, C) } catch (e) {
                        if (u) throw e;
                        C(-1, e)
                    }
                } else C(-1, "No Transport");

                function C(e, t, s, c) {
                    var p, f, b, w, x, T = t;
                    u || (u = !0, a && n.clearTimeout(a), r = void 0, i = c || "", S.readyState = 0 < e ? 4 : 0, p = 200 <= e && e < 300 || 304 === e, s && (w = function(e, t, n) {
                        for (var r, o, i, s, a = e.contents, c = e.dataTypes;
                            "*" === c[0];) c.shift(), void 0 === r && (r = e.mimeType || t.getResponseHeader("Content-Type"));
                        if (r)
                            for (o in a)
                                if (a[o] && a[o].test(r)) { c.unshift(o); break }
                        if (c[0] in n) i = c[0];
                        else {
                            for (o in n) {
                                if (!c[0] || e.converters[o + " " + c[0]]) { i = o; break }
                                s || (s = o)
                            }
                            i = i || s
                        }
                        if (i) return i !== c[0] && c.unshift(i), n[i]
                    }(d, S, s)), !p && -1 < E.inArray("script", d.dataTypes) && (d.converters["text script"] = function() {}), w = function(e, t, n, r) {
                        var o, i, s, a, c, u = {},
                            l = e.dataTypes.slice();
                        if (l[1])
                            for (s in e.converters) u[s.toLowerCase()] = e.converters[s];
                        for (i = l.shift(); i;)
                            if (e.responseFields[i] && (n[e.responseFields[i]] = t), !c && r && e.dataFilter && (t = e.dataFilter(t, e.dataType)), c = i, i = l.shift())
                                if ("*" === i) i = c;
                                else if ("*" !== c && c !== i) {
                            if (!(s = u[c + " " + i] || u["* " + i]))
                                for (o in u)
                                    if ((a = o.split(" "))[1] === i && (s = u[c + " " + a[0]] || u["* " + a[0]])) {!0 === s ? s = u[o] : !0 !== u[o] && (i = a[0], l.unshift(a[1])); break }
                            if (!0 !== s)
                                if (s && e.throws) t = s(t);
                                else try { t = s(t) } catch (e) { return { state: "parsererror", error: s ? e : "No conversion from " + c + " to " + i } }
                        }
                        return { state: "success", data: t }
                    }(d, w, S, p), p ? (d.ifModified && ((x = S.getResponseHeader("Last-Modified")) && (E.lastModified[o] = x), (x = S.getResponseHeader("etag")) && (E.etag[o] = x)), 204 === e || "HEAD" === d.type ? T = "nocontent" : 304 === e ? T = "notmodified" : (T = w.state, f = w.data, p = !(b = w.error))) : (b = T, !e && T || (T = "error", e < 0 && (e = 0))), S.status = e, S.statusText = (t || T) + "", p ? v.resolveWith(h, [f, T, S]) : v.rejectWith(h, [S, T, b]), S.statusCode(m), m = void 0, l && y.trigger(p ? "ajaxSuccess" : "ajaxError", [S, d, p ? f : b]), g.fireWith(h, [S, T]), l && (y.trigger("ajaxComplete", [S, d]), --E.active || E.event.trigger("ajaxStop")))
                }
                return S
            },
            getJSON: function(e, t, n) { return E.get(e, t, n, "json") },
            getScript: function(e, t) { return E.get(e, void 0, t, "script") }
        }), E.each(["get", "post"], (function(e, t) { E[t] = function(e, n, r, o) { return g(n) && (o = o || r, r = n, n = void 0), E.ajax(E.extend({ url: e, type: t, dataType: o, data: n, success: r }, E.isPlainObject(e) && e)) } })), E.ajaxPrefilter((function(e) { var t; for (t in e.headers) "content-type" === t.toLowerCase() && (e.contentType = e.headers[t] || "") })), E._evalUrl = function(e, t, n) { return E.ajax({ url: e, type: "GET", dataType: "script", cache: !0, async: !1, global: !1, converters: { "text script": function() {} }, dataFilter: function(e) { E.globalEval(e, t, n) } }) }, E.fn.extend({
            wrapAll: function(e) { var t; return this[0] && (g(e) && (e = e.call(this[0])), t = E(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map((function() { for (var e = this; e.firstElementChild;) e = e.firstElementChild; return e })).append(this)), this },
            wrapInner: function(e) {
                return g(e) ? this.each((function(t) { E(this).wrapInner(e.call(this, t)) })) : this.each((function() {
                    var t = E(this),
                        n = t.contents();
                    n.length ? n.wrapAll(e) : t.append(e)
                }))
            },
            wrap: function(e) { var t = g(e); return this.each((function(n) { E(this).wrapAll(t ? e.call(this, n) : e) })) },
            unwrap: function(e) { return this.parent(e).not("body").each((function() { E(this).replaceWith(this.childNodes) })), this }
        }), E.expr.pseudos.hidden = function(e) { return !E.expr.pseudos.visible(e) }, E.expr.pseudos.visible = function(e) { return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length) }, E.ajaxSettings.xhr = function() { try { return new n.XMLHttpRequest } catch (e) {} };
        var Yt = { 0: 200, 1223: 204 },
            Gt = E.ajaxSettings.xhr();
        v.cors = !!Gt && "withCredentials" in Gt, v.ajax = Gt = !!Gt, E.ajaxTransport((function(e) {
            var t, r;
            if (v.cors || Gt && !e.crossDomain) return {
                send: function(o, i) {
                    var s, a = e.xhr();
                    if (a.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields)
                        for (s in e.xhrFields) a[s] = e.xhrFields[s];
                    for (s in e.mimeType && a.overrideMimeType && a.overrideMimeType(e.mimeType), e.crossDomain || o["X-Requested-With"] || (o["X-Requested-With"] = "XMLHttpRequest"), o) a.setRequestHeader(s, o[s]);
                    t = function(e) { return function() { t && (t = r = a.onload = a.onerror = a.onabort = a.ontimeout = a.onreadystatechange = null, "abort" === e ? a.abort() : "error" === e ? "number" != typeof a.status ? i(0, "error") : i(a.status, a.statusText) : i(Yt[a.status] || a.status, a.statusText, "text" !== (a.responseType || "text") || "string" != typeof a.responseText ? { binary: a.response } : { text: a.responseText }, a.getAllResponseHeaders())) } }, a.onload = t(), r = a.onerror = a.ontimeout = t("error"), void 0 !== a.onabort ? a.onabort = r : a.onreadystatechange = function() { 4 === a.readyState && n.setTimeout((function() { t && r() })) }, t = t("abort");
                    try { a.send(e.hasContent && e.data || null) } catch (o) { if (t) throw o }
                },
                abort: function() { t && t() }
            }
        })), E.ajaxPrefilter((function(e) { e.crossDomain && (e.contents.script = !1) })), E.ajaxSetup({ accepts: { script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript" }, contents: { script: /\b(?:java|ecma)script\b/ }, converters: { "text script": function(e) { return E.globalEval(e), e } } }), E.ajaxPrefilter("script", (function(e) { void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET") })), E.ajaxTransport("script", (function(e) { var t, n; if (e.crossDomain || e.scriptAttrs) return { send: function(r, o) { t = E("<script>").attr(e.scriptAttrs || {}).prop({ charset: e.scriptCharset, src: e.url }).on("load error", n = function(e) { t.remove(), n = null, e && o("error" === e.type ? 404 : 200, e.type) }), b.head.appendChild(t[0]) }, abort: function() { n && n() } } }));
        var Xt, Vt = [],
            Jt = /(=)\?(?=&|$)|\?\?/;
        E.ajaxSetup({ jsonp: "callback", jsonpCallback: function() { var e = Vt.pop() || E.expando + "_" + kt.guid++; return this[e] = !0, e } }), E.ajaxPrefilter("json jsonp", (function(e, t, r) { var o, i, s, a = !1 !== e.jsonp && (Jt.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Jt.test(e.data) && "data"); if (a || "jsonp" === e.dataTypes[0]) return o = e.jsonpCallback = g(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, a ? e[a] = e[a].replace(Jt, "$1" + o) : !1 !== e.jsonp && (e.url += (jt.test(e.url) ? "&" : "?") + e.jsonp + "=" + o), e.converters["script json"] = function() { return s || E.error(o + " was not called"), s[0] }, e.dataTypes[0] = "json", i = n[o], n[o] = function() { s = arguments }, r.always((function() { void 0 === i ? E(n).removeProp(o) : n[o] = i, e[o] && (e.jsonpCallback = t.jsonpCallback, Vt.push(o)), s && g(i) && i(s[0]), s = i = void 0 })), "script" })), v.createHTMLDocument = ((Xt = b.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === Xt.childNodes.length), E.parseHTML = function(e, t, n) { return "string" != typeof e ? [] : ("boolean" == typeof t && (n = t, t = !1), t || (v.createHTMLDocument ? ((r = (t = b.implementation.createHTMLDocument("")).createElement("base")).href = b.location.href, t.head.appendChild(r)) : t = b), i = !n && [], (o = D.exec(e)) ? [t.createElement(o[1])] : (o = Te([e], t, i), i && i.length && E(i).remove(), E.merge([], o.childNodes))); var r, o, i }, E.fn.load = function(e, t, n) {
            var r, o, i, s = this,
                a = e.indexOf(" ");
            return -1 < a && (r = bt(e.slice(a)), e = e.slice(0, a)), g(t) ? (n = t, t = void 0) : t && "object" == typeof t && (o = "POST"), 0 < s.length && E.ajax({ url: e, type: o || "GET", dataType: "html", data: t }).done((function(e) { i = arguments, s.html(r ? E("<div>").append(E.parseHTML(e)).find(r) : e) })).always(n && function(e, t) { s.each((function() { n.apply(this, i || [e.responseText, t, e]) })) }), this
        }, E.expr.pseudos.animated = function(e) { return E.grep(E.timers, (function(t) { return e === t.elem })).length }, E.offset = {
            setOffset: function(e, t, n) {
                var r, o, i, s, a, c, u = E.css(e, "position"),
                    l = E(e),
                    p = {};
                "static" === u && (e.style.position = "relative"), a = l.offset(), i = E.css(e, "top"), c = E.css(e, "left"), ("absolute" === u || "fixed" === u) && -1 < (i + c).indexOf("auto") ? (s = (r = l.position()).top, o = r.left) : (s = parseFloat(i) || 0, o = parseFloat(c) || 0), g(t) && (t = t.call(e, n, E.extend({}, a))), null != t.top && (p.top = t.top - a.top + s), null != t.left && (p.left = t.left - a.left + o), "using" in t ? t.using.call(e, p) : ("number" == typeof p.top && (p.top += "px"), "number" == typeof p.left && (p.left += "px"), l.css(p))
            }
        }, E.fn.extend({
            offset: function(e) { if (arguments.length) return void 0 === e ? this : this.each((function(t) { E.offset.setOffset(this, e, t) })); var t, n, r = this[0]; return r ? r.getClientRects().length ? (t = r.getBoundingClientRect(), n = r.ownerDocument.defaultView, { top: t.top + n.pageYOffset, left: t.left + n.pageXOffset }) : { top: 0, left: 0 } : void 0 },
            position: function() {
                if (this[0]) {
                    var e, t, n, r = this[0],
                        o = { top: 0, left: 0 };
                    if ("fixed" === E.css(r, "position")) t = r.getBoundingClientRect();
                    else {
                        for (t = this.offset(), n = r.ownerDocument, e = r.offsetParent || n.documentElement; e && (e === n.body || e === n.documentElement) && "static" === E.css(e, "position");) e = e.parentNode;
                        e && e !== r && 1 === e.nodeType && ((o = E(e).offset()).top += E.css(e, "borderTopWidth", !0), o.left += E.css(e, "borderLeftWidth", !0))
                    }
                    return { top: t.top - o.top - E.css(r, "marginTop", !0), left: t.left - o.left - E.css(r, "marginLeft", !0) }
                }
            },
            offsetParent: function() { return this.map((function() { for (var e = this.offsetParent; e && "static" === E.css(e, "position");) e = e.offsetParent; return e || se })) }
        }), E.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, (function(e, t) {
            var n = "pageYOffset" === t;
            E.fn[e] = function(r) {
                return z(this, (function(e, r, o) {
                    var i;
                    if (m(e) ? i = e : 9 === e.nodeType && (i = e.defaultView), void 0 === o) return i ? i[t] : e[r];
                    i ? i.scrollTo(n ? i.pageXOffset : o, n ? o : i.pageYOffset) : e[r] = o
                }), e, r, arguments.length)
            }
        })), E.each(["top", "left"], (function(e, t) { E.cssHooks[t] = ze(v.pixelPosition, (function(e, n) { if (n) return n = Ue(e, t), Me.test(n) ? E(e).position()[t] + "px" : n })) })), E.each({ Height: "height", Width: "width" }, (function(e, t) {
            E.each({ padding: "inner" + e, content: t, "": "outer" + e }, (function(n, r) {
                E.fn[r] = function(o, i) {
                    var s = arguments.length && (n || "boolean" != typeof o),
                        a = n || (!0 === o || !0 === i ? "margin" : "border");
                    return z(this, (function(t, n, o) { var i; return m(t) ? 0 === r.indexOf("outer") ? t["inner" + e] : t.document.documentElement["client" + e] : 9 === t.nodeType ? (i = t.documentElement, Math.max(t.body["scroll" + e], i["scroll" + e], t.body["offset" + e], i["offset" + e], i["client" + e])) : void 0 === o ? E.css(t, n, a) : E.style(t, n, o, a) }), t, s ? o : void 0, s)
                }
            }))
        })), E.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], (function(e, t) { E.fn[t] = function(e) { return this.on(t, e) } })), E.fn.extend({ bind: function(e, t, n) { return this.on(e, null, t, n) }, unbind: function(e, t) { return this.off(e, null, t) }, delegate: function(e, t, n, r) { return this.on(t, e, n, r) }, undelegate: function(e, t, n) { return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n) }, hover: function(e, t) { return this.mouseenter(e).mouseleave(t || e) } }), E.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), (function(e, t) { E.fn[t] = function(e, n) { return 0 < arguments.length ? this.on(t, null, e, n) : this.trigger(t) } }));
        var Qt = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
        E.proxy = function(e, t) { var n, r, o; if ("string" == typeof t && (n = e[t], t = e, e = n), g(e)) return r = a.call(arguments, 2), (o = function() { return e.apply(t || this, r.concat(a.call(arguments))) }).guid = e.guid = e.guid || E.guid++, o }, E.holdReady = function(e) { e ? E.readyWait++ : E.ready(!0) }, E.isArray = Array.isArray, E.parseJSON = JSON.parse, E.nodeName = O, E.isFunction = g, E.isWindow = m, E.camelCase = V, E.type = T, E.now = Date.now, E.isNumeric = function(e) { var t = E.type(e); return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e)) }, E.trim = function(e) { return null == e ? "" : (e + "").replace(Qt, "") }, void 0 === (r = function() { return E }.apply(t, [])) || (e.exports = r);
        var Kt = n.jQuery,
            Zt = n.$;
        return E.noConflict = function(e) { return n.$ === E && (n.$ = Zt), e && n.jQuery === E && (n.jQuery = Kt), E }, void 0 === o && (n.jQuery = n.$ = E), E
    }))
}, function(e, t, n) {
    n(11), n(12);
    var r = n(25).Subject;
    n(0).Observable;
    n(36), n(39);
    var o = n(9);
    n(42), o(document).ready((function() {
        o(".link").on("click", (function(t) {
            t.preventDefault();
            var n = o(this).attr("href");
            "#livechat" == n && o("#livechat-embed").html("<iframe border='0' src='https://helpline.homecaremedical.co.nz/ShineChat/'></iframe>"),
                function(t) { e.next(t) }(n)
        }));
        var e = new r;
        e.subscribe({
            next: function(e) {
                ! function(e) {
                    var t = o(".menu-button");
                    switch (t.addClass("open").show(), e) {
                        case "#menu":
                            t.hide();
                            break;
                        case "#home":
                            t.removeClass("open")
                    }
                }(e), o(".template").hide(), o(e).show()
            }
        }), e.next("#home")
    }))
}, function(e, t, n) {}, function(e, t, n) {
    var r = n(13),
        o = n(14),
        i = n(15),
        s = n(16),
        a = n(17),
        c = n(18),
        u = n(19),
        l = n(20),
        p = n(21),
        f = n(22),
        d = n(23),
        h = n(24),
        y = r(o),
        v = r(i),
        g = r(s),
        m = r(a),
        b = r(c),
        w = r(u),
        x = r(l),
        T = r(p),
        S = '<!doctype html> <html lang=en> <head> <meta charset=UTF-8> <title>Shielded</title> </head> <body> <div class=wrapper> <nav class="header ng-scope"> <a class="menu-button menu-but link" href=#menu></a> <a class="wr-button menu-but link" href=#home><img src=' + y + ' alt=""></a> </nav> <div class=state> <div id=home class=template> <div class="content no-scroll" ng-controller=animationController> <div class=canvas-container> <img class=gold-logo src=' + v + ' alt=""/> </div> <div class=animation-container> <div class="slide-2 slide fade-in"> <div class=entrance> <p> If you are experiencing family violence, don\'t worry, the information within this pop-up won\'t appear in your browser\'s history. </p> <a href=#menu class="enter link">enter</a> </div> </div> </div> <a href=#privacy class="privacy link">Privacy policy</a> </div> </div> <div id=shielded class=template> <div class=content> <div class=text-container> <p>Weâ€™ve made asking for help safer than ever.</p> <p>Join us in standing up against domestic violence and making more places of refuge across the internet.</p> <p> If you, your business or your agency want to have The Shielded Site tab on your site weâ€™ve made adding it very easy. </p> <a href=https://shielded.co.nz target=_blank> Click here to find out more. (WARNING: this will take you away from our shielded portal.) </a> </div> </div> </div> <div id=answers class=template> <div class="content answer-content"> <div class="image-container answers"> <img class=safe-online src=' + g + " alt=\"\"/> </div> <h1 class=\"sub-title answers\"> NEED MORE ANSWERS </h1> <div class=text-container> <p>If you are living in fear in your relationship or in your family, there are so many ways we can help you right now. You wonâ€™t be turned away even if you donâ€™t have children, a NZ visa, or money. If you still have more questions have a read below and contact us when youâ€™re ready.</p> <p class=scaled> Iâ€™m ready to talk now. </p> <p> You can call our 24-hour support and crisis line on 0800 REFUGE (733843). Or, if you prefer, you can click <a class=link href=#help>here</a> and contact us discretely through our contact form and we will email you back as soon as possible. </p> <p class=scaled> What will I do for money? </p> <p> There are a number of benefits and allowances you may be eligible for if you are a victim of domestic violence in New Zealand. We can help you better understand your options once you make contact. </p> <p class=scaled> I havenâ€™t been beaten up, can Womenâ€™s Refuge still help me? </p> <p> We support women who have experienced any form of domestic violence: verbal, psychological/emotional, sexual, and financial as well as physical. In fact, psychological/emotional abuse is the most common form of domestic violence. </p> <p class=scaled> How much does it cost to stay? </p> <p> Women's Refuge support and advocacy services are free. In the safe house, rent is usually charged once your financial situation is sorted out. Safety is our main concern. You won't be turned away if you don't have any money. </p> <p class=scaled> How long can I stay in a safe house? </p> <p>Some women only stay a night or two, while others stay for weeks. You can talk with the advocates at your local refuge about how long you think you need to stay to ensure your safety.</p> <p class=scaled> I donâ€™t live with my partner, but he is abusing me. Can you still help me? </p> <p> Yes, you donâ€™t have to be living with your partner to experience domestic violence and you can still call us. </p> <p class=scaled> What happens if I haven't got any clothes or food? </p> <p> Women's Refuge has clothing that you can have. Weâ€™ve also got toys and books, formula and nappies. You are welcome to use our emergency food until you get your financial situation sorted out. </p> <p class=scaled> Will other people be there? </p> <p> Safe houses usually have other women, including women with their children, staying there. Refuge advocates are around during the day. </p> <p class=scaled> How will I get my kids to school? </p> <p> The advocates at your local refuge will help you work out transport for your children, or help with changing schools. </p> <p class=scaled> Can Women's Refuge help me if I stay in my own house? </p> <p> Yes, we can provide all the same support and advocacy for you no matter where you choose to live. You may be eligible to access support through the Whanau Protect service. </p> <p class=scaled> I'm living in a rural area. Can you still help me? </p> <p> Yes. Find your local refuge and they will be able to arrange support, advocacy and transport for you. </p> <p class=scaled> Can Women's Refuge help around issues with children? </p> <p>Yes. We can provide support and advocacy around matters to do with custody, access and care. </p> </div> </div> </div> <div id=safe class=template> <div class=content> <div class=\"image-container safe-online\"> <img class=safe-online src=" + m + ' alt=""/> </div> <h1 class="sub-title safe-online"> BEING SAFE ONLINE </h1> <div class=text-container> <p>The safest way to browse the internet if you suspect your browsing history is being monitored, is to use your browserâ€™s private or incognito mode. </p> <p>If you suspect your device has been compromised by spyware, then you should use consider using another device as some spyware may still be able to monitor icognito sessions.</p> <p>To activate a private browsing session, follow the instructions below.</p> <p class=scaled>Safari</p> <p>Open <strong>Safari</strong> > go to the <strong>File</strong> menu > select <strong>New Private Window</strong></p> <p>When finished, donâ€™t forget to close your browser window to ensure your safety and privacy.</p> <p class=scaled> Chrome </p> <p>Open <strong>Chrome</strong> > go to the <strong>triple-dot</strong> menu (top right of your browser\'s window) > select <strong>New Incognito Window</strong></p> <p class=scaled>Internet Explorer</p> <p>Open <strong>IE</strong> > click the <strong>Tools</strong> button > select <strong>Safety</strong> > and then click <strong>InPrivate Browsing</strong></p> <p class=scaled>Mozilla Firefox</p> <p>Open <strong>Firefox</strong> > click the <strong>menu</strong> button â˜° > and then click <strong>New Private Window</strong></p> <p>You should see a message in the new window saying that you are now browsing privately. </p> <p>When finished, donâ€™t forget to close your browser window to ensure your safety and privacy.</p> </div> </div> </div> <div id=out class=template> <div class=content> <div class="image-container getting-out"> <img class=getting-out src=' + b + ' alt=""/> </div> <h1 class="sub-title getting-out"> Getting out </h1> <div class=text-container> <p>The most important thing is for you and your children to get out safely. It is important to know that leaving a violent relationship can be one of the most dangerous times for women and children so it is important to make a safety plan around leaving and keep your plans confidential. Below are some tips to help you make a plan. </p> <ul class=dash-list> <li><p>If you can, pack a bag with bare necessities and important documents that you can leave with someone you trust. Include important documents such as passport, birth certificate, bank account details, driverâ€™s licence, and bank cards and other things like medicines.</p></li> <li><p>Know abuser\'s schedule and safe times to leave.</p></li> <li><p><a href=#help class=link>Contact us</a> for guidance or a safe place to stay for you and your children.</p></li> </ul> </div> </div> </div> <div id=livechat class=template> <div class=content> <div id=livechat-embed> </div> </div> </div> <div id=help class=template> <div class=content> <div class="image-container getting-help"> <img class=danger src=' + w + ' alt=""/> </div> <h1 class="sub-title getting-help"> Getting help </h1> <div class=form-container> <p> We warmly welcome all women and their children to access our support, advocacy and crisis accommodation. If you need help or have questions, use our live chat to get in touch. </p> <div> <div class="menu-container center"> <a href=#livechat class=link> <div class="image-container get-help"><img src=' + w + ' /></div> <p>Live Chat</p> </a> </div> </div> </div> </div> </div> <div id=plan class=template> <div class=content> <div class="image-container making-plan"> <img class=making-plan src=' + x + ' alt=""/> </div> <h1 class="sub-title making-plan"> making a plan </h1> <div class=text-container> <p>The safety of you and your children (if you have them) will be your primary concern. If youâ€™re not ready or cannot safely leave, here are some things you can do to stay safe now.</p> <ul class=dash-list> <li> <p>Make a safety plan with the guidance of a refuge advocate.</p> </li> <li> <p>Get yourself a pre-paid phone; keep it charged and safe.</p> </li> <li> <p>Keep photocopies of important documents (passport, birth certificate, bank account details, medical notes, driver\'s licence, etc) and store these at the home of a supportive friend or family member.</p> </li> <li> <p>Keep a journal of all violent incidents, noting dates and events.</p> </li> <li> <p>If you can, open your own bank account and try to save some money.</p> </li> <li> <p>If you have pets you are worried about, consider them in your safety plan.</p> </li> </ul> </div> </div> </div> <div id=privacy class=template> <div class=content> <h1 class="sub-title answers"> Privacy Policy â€“ The Shielded Site Application. </h1> <div class=text-container> <p class=scaled> General </p> <p> In this privacy policy, the terms â€˜<strong>NCIWR</strong>â€™, â€˜<strong>we</strong>â€™, â€˜<strong>us</strong>â€™, and â€˜<strong>our</strong>â€™ refer to National Collective of Independent Womenâ€™s Refuges Inc. NCIWR operates this web application at https://d3f5l8ze0o4j2m.cloudfront.net (â€˜<strong>this web application</strong>â€™). </p> <p> This privacy policy explains how we may collect, store, use, and disclose personal information that we collect and that you provide to us. By using this web application you acknowledge that we may collect, store, use, and disclose your personal information in the manner set out in this privacy policy. </p> <p class=scaled> Collection of personal information </p> <p> We may collect personal information from you when you use this web application, for example when you make a request for contact on this web application. </p> <p> You may decide not to provide your personal information to us. However, if you do not provide it, we may not be able to provide you with access to certain information or services. For example, we may be unable to make contact with you if you do not provide us with your contact information. </p> <p class=scaled> Automated collection of non-personal information </p> <p> When you visit this web application, we will not add traceable elements (such as cookies, sessions, and usage monitoring software) to your browser or device. </p> <p class=scaled> Use and disclosure </p> <p> We will not use or disclose your personal information except in accordance with this privacy policy or the Privacy Act 1993. We may use your personal information to: </p> <ul> <li> <p>assist in providing information and services requested by you;</p> </li> <li> <p>communicate with you</p> </li> </ul> <p> Your personal information will only be made available internally for the above purposes. We will not disclose your personal information to third parties. We will only use or disclose personal information that you have provided to us, or which we have obtained about you: </p> <ul> <li> <p>for the above-mentioned purposes;</p> </li> <li> <p>if you have otherwise authorised us to do so;</p> </li> <li> <p>if we have given you notification of the intended use or disclosure and you have not objected to that use or disclosure;</p> </li> <li> <p>if we believe that the use or disclosure is reasonably necessary to assist a law enforcement agency or an agency responsible for national security in the performance of their functions; </p> </li> <li> <p>if we believe that the use or disclosure is reasonably necessary to enforce any legal rights we may have, or is reasonably necessary to protect the rights, property and safety of us, our customers and users, or others; </p> </li> <li> <p>if we are required or permitted by law to disclose the information; or</p> </li> <li> <p>to another entity that carries on the business of operating this web application.</p> </li> </ul> <p class=scaled> Storage and security </p> <p> All personal information collected on this web application is collected and held by NCIWR. We will endeavour to protect your personal information that is held by us from unauthorised access, use, disclosure, alteration, or destruction. </p> <p class=scaled> Third party service providers </p> <p> This website may be hosted by one or more third party service providers (â€˜service providersâ€™) who enable us to provide this web application. You acknowledge and agree that any personal information that may be collected on this web application may also be held and used by our service providers on our behalf. Any information collected will be securely sent and securely stored on a server. </p> <p class=scaled> Third party websites </p> <p> This web application may be hosted by websites operated by third parties. We are not responsible for the content of such websites, or the manner in which those websites collect, store, use, or distribute any personal information you provide. When you visit third party websites from hyperlinks displayed on this web application, we encourage you to review the privacy statements of those websites so that you can understand how the personal information you provide may be collected, stored, used, and distributed. </p> <p class=scaled> Right to access and correct </p> <p> You may request access to, or correction of, any personal information we hold about you by contacting us as follows: </p> <table> <tr> <td>Email:</td> <td>info@refuge.org.nz</td> </tr> <tr> <td>Post:</td> <td>Privacy Officer <br/> NCIWR <br/> PO Box 27-078 <br/> Marion Square <br/> Wellington 6141 </td> </tr> </table> <p>To ensure that the contact information we hold about you is accurate and current, please notify us of any changes to such information as soon as possible.</p> <p class=scaled> Contacting NCIWR </p> <p> Any emergency relating to domestic violence should be directed to 111 for New Zealand Police assistance. </p> <p> If you request assistance through this website, we will endeavour to respond as soon as we can. If you require advocacy services phone 0800 REFUGE or 0800 733 843 to talk to a refuge in your area within New Zealand. All member refuges of NCIWR are listed on our main website (<a href=https://www.womensrefuge.org.nz>www.womensrefuge.org.nz</a>). If you do visit the Womenâ€™s Refuge Website, please note that it is a traceable site so we recommend you use the online safety tips found on this web application to visit <a href=https://www.womensrefuge.org.nz>www.womensrefuge.org.nz</a> safely. </p> <p>Advocacy services are available at member refuges. Your call and information will be treated in confidence and privacy.</p> <p class=scaled> Changes to our privacy policy </p> <p> We reserve the right, at our discretion, to alter this privacy policy at any time. Changes to this privacy policy will take effect immediately once they are published on this web application. Please check this privacy policy regularly for modifications and updates. If you continue to use this web application or if you provide any personal information after we post changes to this privacy policy, this will indicate your acceptance of any such changes. </p> <p>This privacy policy was last updated on 6 October 2015.</p> </div> </div> </div> <div id=urgent class=template> <div class=content> <div class=image-container> <img class=danger src=' + T + ' alt=""/> </div> <h1 class=sub-title> If Youâ€™re In <br/> Immediate danger <br/> CALL 111 IMMEDIATELY </h1> <p class=indented-text>If you fear for your safety:</p> <ol> <li>Run outside and head for where there are other people. </li> <li>Ask someone to call 111</li> <li>If you have children take them with you if you can</li> <li>Don\'t stop to get anything else</li> </ol> </div> </div> <div id=menu class=template> <div class="content no-scroll"> <div class="menu-container left"> <a href=#urgent class=link> <div class="image-container danger"><img src=' + T + " /></div> <p>IN DANGER</p> </a> <a href=#out class=link> <div class=image-container><img src=" + r(f) + ' /></div> <p>Getting out</p> </a> <a href=#safe class=link> <div class="image-container safety"><img src=' + r(d) + ' /></div> <p>ONLINE SAFETY</p> </a> </div> <div class="menu-container right"> <a href=#help class=link> <div class="image-container get-help"><img src=' + w + " /></div> <p>GETTING HELP</p> </a> <a href=#plan class=link> <div class=image-container><img src=" + x + ' /></div> <p>Making a plan</p> </a> <a href=#answers class=link> <div class="image-container answers"><img src=' + r(h) + ' /></div> <p>NEED ANSWERS</p> </a> </div> <a href=#shielded class="link brown-link"> <h1 class=sub-title>ABOUT THE SHIELDED SITE <br></h1> </a> </div> </div> </div> </div> </body> </html>';
    e.exports = S
}, function(e, t, n) {
    "use strict";
    e.exports = function(e, t) { return t || (t = {}), "string" != typeof(e = e && e.__esModule ? e.default : e) ? e : (t.hash && (e += t.hash), t.maybeNeedQuotes && /[\t\n\f\r "'=<>`]/.test(e) ? '"'.concat(e, '"') : e) }
}, function(e, t, n) {
    "use strict";
    n.r(t), t.default = n.p + "img/wr-small.png?0ee002942d09f4f56a6e817c22846968"
}, function(e, t, n) {
    "use strict";
    n.r(t), t.default = n.p + "img/shielded-logo.png?e76d614212318f30f11a0fac1cec1088"
}, function(e, t, n) {
    "use strict";
    n.r(t), t.default = n.p + "img/faqs.svg?c3a5be661b890a7c1cae5df1f164271d"
}, function(e, t, n) {
    "use strict";
    n.r(t), t.default = n.p + "img/sml-logo.svg?4da65dfa9c8bbb009520265439ebe393"
}, function(e, t, n) {
    "use strict";
    n.r(t), t.default = n.p + "img/med-logo.svg?b95d832f4f3b77bfcd220207cc941c71"
}, function(e, t, n) {
    "use strict";
    n.r(t), t.default = n.p + "img/dv.svg?6fbebd47c5d53251ac67f0043b11a5b7"
}, function(e, t, n) {
    "use strict";
    n.r(t), t.default = n.p + "img/macking-plan.svg?c070139342c915e040fe68bc79648ff9"
}, function(e, t, n) {
    "use strict";
    n.r(t), t.default = n.p + "img/alert.svg?8b4ef298d65dc973f21802844d55ba68"
}, function(e, t, n) {
    "use strict";
    n.r(t), t.default = n.p + "img/getting-out.svg?b95d832f4f3b77bfcd220207cc941c71"
}, function(e, t, n) {
    "use strict";
    n.r(t), t.default = n.p + "img/safe-online.svg?4da65dfa9c8bbb009520265439ebe393"
}, function(e, t, n) {
    "use strict";
    n.r(t), t.default = n.p + "img/answers.svg?c3a5be661b890a7c1cae5df1f164271d"
}, function(e, t, n) {
    "use strict";
    var r = this && this.__extends || function(e, t) {
            for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);

            function r() { this.constructor = e }
            e.prototype = null === t ? Object.create(t) : (r.prototype = t.prototype, new r)
        },
        o = n(0),
        i = n(3),
        s = n(1),
        a = n(34),
        c = n(35),
        u = n(6),
        l = function(e) {
            function t(t) { e.call(this, t), this.destination = t }
            return r(t, e), t
        }(i.Subscriber);
    t.SubjectSubscriber = l;
    var p = function(e) {
        function t() { e.call(this), this.observers = [], this.closed = !1, this.isStopped = !1, this.hasError = !1, this.thrownError = null }
        return r(t, e), t.prototype[u.rxSubscriber] = function() { return new l(this) }, t.prototype.lift = function(e) { var t = new f(this, this); return t.operator = e, t }, t.prototype.next = function(e) {
            if (this.closed) throw new a.ObjectUnsubscribedError;
            if (!this.isStopped)
                for (var t = this.observers, n = t.length, r = t.slice(), o = 0; o < n; o++) r[o].next(e)
        }, t.prototype.error = function(e) {
            if (this.closed) throw new a.ObjectUnsubscribedError;
            this.hasError = !0, this.thrownError = e, this.isStopped = !0;
            for (var t = this.observers, n = t.length, r = t.slice(), o = 0; o < n; o++) r[o].error(e);
            this.observers.length = 0
        }, t.prototype.complete = function() {
            if (this.closed) throw new a.ObjectUnsubscribedError;
            this.isStopped = !0;
            for (var e = this.observers, t = e.length, n = e.slice(), r = 0; r < t; r++) n[r].complete();
            this.observers.length = 0
        }, t.prototype.unsubscribe = function() { this.isStopped = !0, this.closed = !0, this.observers = null }, t.prototype._trySubscribe = function(t) { if (this.closed) throw new a.ObjectUnsubscribedError; return e.prototype._trySubscribe.call(this, t) }, t.prototype._subscribe = function(e) { if (this.closed) throw new a.ObjectUnsubscribedError; return this.hasError ? (e.error(this.thrownError), s.Subscription.EMPTY) : this.isStopped ? (e.complete(), s.Subscription.EMPTY) : (this.observers.push(e), new c.SubjectSubscription(this, e)) }, t.prototype.asObservable = function() { var e = new o.Observable; return e.source = this, e }, t.create = function(e, t) { return new f(e, t) }, t
    }(o.Observable);
    t.Subject = p;
    var f = function(e) {
        function t(t, n) { e.call(this), this.destination = t, this.source = n }
        return r(t, e), t.prototype.next = function(e) {
            var t = this.destination;
            t && t.next && t.next(e)
        }, t.prototype.error = function(e) {
            var t = this.destination;
            t && t.error && this.destination.error(e)
        }, t.prototype.complete = function() {
            var e = this.destination;
            e && e.complete && this.destination.complete()
        }, t.prototype._subscribe = function(e) { return this.source ? this.source.subscribe(e) : s.Subscription.EMPTY }, t
    }(p);
    t.AnonymousSubject = f
}, function(e, t) {
    var n;
    n = function() { return this }();
    try { n = n || new Function("return this")() } catch (e) { "object" == typeof window && (n = window) }
    e.exports = n
}, function(e, t, n) {
    "use strict";
    var r = n(3),
        o = n(6),
        i = n(8);
    t.toSubscriber = function(e, t, n) { if (e) { if (e instanceof r.Subscriber) return e; if (e[o.rxSubscriber]) return e[o.rxSubscriber]() } return e || t || n ? new r.Subscriber(e, t, n) : new r.Subscriber(i.empty) }
}, function(e, t, n) {
    "use strict";
    t.isArray = Array.isArray || function(e) { return e && "number" == typeof e.length }
}, function(e, t, n) {
    "use strict";
    t.isObject = function(e) { return null != e && "object" == typeof e }
}, function(e, t, n) {
    "use strict";
    var r = this && this.__extends || function(e, t) {
            for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);

            function r() { this.constructor = e }
            e.prototype = null === t ? Object.create(t) : (r.prototype = t.prototype, new r)
        },
        o = function(e) {
            function t(t) {
                e.call(this), this.errors = t;
                var n = Error.call(this, t ? t.length + " errors occurred during unsubscription:\n  " + t.map((function(e, t) { return t + 1 + ") " + e.toString() })).join("\n  ") : "");
                this.name = n.name = "UnsubscriptionError", this.stack = n.stack, this.message = n.message
            }
            return r(t, e), t
        }(Error);
    t.UnsubscriptionError = o
}, function(e, t, n) {
    "use strict";
    var r = n(2);

    function o(e) { var t, n = e.Symbol; return "function" == typeof n ? n.observable ? t = n.observable : (t = n("observable"), n.observable = t) : t = "@@observable", t }
    t.getSymbolObservable = o, t.observable = o(r.root), t.$$observable = t.observable
}, function(e, t, n) {
    "use strict";
    var r = n(33);

    function o(e) { return e ? 1 === e.length ? e[0] : function(t) { return e.reduce((function(e, t) { return t(e) }), t) } : r.noop }
    t.pipe = function() { for (var e = [], t = 0; t < arguments.length; t++) e[t - 0] = arguments[t]; return o(e) }, t.pipeFromArray = o
}, function(e, t, n) {
    "use strict";
    t.noop = function() {}
}, function(e, t, n) {
    "use strict";
    var r = this && this.__extends || function(e, t) {
            for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);

            function r() { this.constructor = e }
            e.prototype = null === t ? Object.create(t) : (r.prototype = t.prototype, new r)
        },
        o = function(e) {
            function t() {
                var t = e.call(this, "object unsubscribed");
                this.name = t.name = "ObjectUnsubscribedError", this.stack = t.stack, this.message = t.message
            }
            return r(t, e), t
        }(Error);
    t.ObjectUnsubscribedError = o
}, function(e, t, n) {
    "use strict";
    var r = this && this.__extends || function(e, t) {
            for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);

            function r() { this.constructor = e }
            e.prototype = null === t ? Object.create(t) : (r.prototype = t.prototype, new r)
        },
        o = function(e) {
            function t(t, n) { e.call(this), this.subject = t, this.subscriber = n, this.closed = !1 }
            return r(t, e), t.prototype.unsubscribe = function() {
                if (!this.closed) {
                    this.closed = !0;
                    var e = this.subject,
                        t = e.observers;
                    if (this.subject = null, t && 0 !== t.length && !e.isStopped && !e.closed) { var n = t.indexOf(this.subscriber); - 1 !== n && t.splice(n, 1) }
                }
            }, t
        }(n(1).Subscription);
    t.SubjectSubscription = o
}, function(e, t, n) {
    "use strict";
    var r = n(0),
        o = n(37);
    r.Observable.fromEvent = o.fromEvent
}, function(e, t, n) {
    "use strict";
    var r = n(38);
    t.fromEvent = r.FromEventObservable.create
}, function(e, t, n) {
    "use strict";
    var r = this && this.__extends || function(e, t) {
            for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);

            function r() { this.constructor = e }
            e.prototype = null === t ? Object.create(t) : (r.prototype = t.prototype, new r)
        },
        o = n(0),
        i = n(7),
        s = n(4),
        a = n(5),
        c = n(1),
        u = Object.prototype.toString;
    var l = function(e) {
        function t(t, n, r, o) { e.call(this), this.sourceObj = t, this.eventName = n, this.selector = r, this.options = o }
        return r(t, e), t.create = function(e, n, r, o) { return s.isFunction(r) && (o = r, r = void 0), new t(e, n, o, r) }, t.setupSubscription = function(e, n, r, o, i) {
            var s;
            if (function(e) { return !!e && "[object NodeList]" === u.call(e) }(e) || function(e) { return !!e && "[object HTMLCollection]" === u.call(e) }(e))
                for (var a = 0, l = e.length; a < l; a++) t.setupSubscription(e[a], n, r, o, i);
            else if (function(e) { return !!e && "function" == typeof e.addEventListener && "function" == typeof e.removeEventListener }(e)) {
                var p = e;
                e.addEventListener(n, r, i), s = function() { return p.removeEventListener(n, r, i) }
            } else if (function(e) { return !!e && "function" == typeof e.on && "function" == typeof e.off }(e)) {
                var f = e;
                e.on(n, r), s = function() { return f.off(n, r) }
            } else {
                if (! function(e) { return !!e && "function" == typeof e.addListener && "function" == typeof e.removeListener }(e)) throw new TypeError("Invalid event target");
                var d = e;
                e.addListener(n, r), s = function() { return d.removeListener(n, r) }
            }
            o.add(new c.Subscription(s))
        }, t.prototype._subscribe = function(e) {
            var n = this.sourceObj,
                r = this.eventName,
                o = this.options,
                s = this.selector,
                c = s ? function() {
                    for (var t = [], n = 0; n < arguments.length; n++) t[n - 0] = arguments[n];
                    var r = i.tryCatch(s).apply(void 0, t);
                    r === a.errorObject ? e.error(a.errorObject.e) : e.next(r)
                } : function(t) { return e.next(t) };
            t.setupSubscription(n, r, c, e, o)
        }, t
    }(o.Observable);
    t.FromEventObservable = l
}, function(e, t, n) {
    "use strict";
    var r = n(0),
        o = n(40);
    r.Observable.prototype.map = o.map
}, function(e, t, n) {
    "use strict";
    var r = n(41);
    t.map = function(e, t) { return r.map(e, t)(this) }
}, function(e, t, n) {
    "use strict";
    var r = this && this.__extends || function(e, t) {
            for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);

            function r() { this.constructor = e }
            e.prototype = null === t ? Object.create(t) : (r.prototype = t.prototype, new r)
        },
        o = n(3);
    t.map = function(e, t) { return function(n) { if ("function" != typeof e) throw new TypeError("argument is not a function. Are you looking for `mapTo()`?"); return n.lift(new i(e, t)) } };
    var i = function() {
        function e(e, t) { this.project = e, this.thisArg = t }
        return e.prototype.call = function(e, t) { return t.subscribe(new s(e, this.project, this.thisArg)) }, e
    }();
    t.MapOperator = i;
    var s = function(e) {
        function t(t, n, r) { e.call(this, t), this.project = n, this.count = 0, this.thisArg = r || this }
        return r(t, e), t.prototype._next = function(e) {
            var t;
            try { t = this.project.call(this.thisArg, e, this.count++) } catch (e) { return void this.destination.error(e) }
            this.destination.next(t)
        }, t
    }(o.Subscriber)
}, function(e, t, n) {
    ! function(e) {
        e.fn.serializeFormJSON = function() {
            var t = {},
                n = this.serializeArray();
            return e.each(n, (function() { t[this.name] ? (t[this.name].push || (t[this.name] = [t[this.name]]), t[this.name].push(this.value || "")) : t[this.name] = this.value || "" })), t
        }
    }(n(9))
}]);